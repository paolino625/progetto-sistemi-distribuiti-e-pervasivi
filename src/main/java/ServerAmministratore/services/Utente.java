package ServerAmministratore.services;

import beans.DroneList_bean;
import beans.StatisticheGlobaliList_bean;
import beans.StatisticheGlobali_bean;
import java.sql.Timestamp;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("utente")
public class Utente {

  // Permette di ricevere lista dei droni presenti nella rete
  @Path("listaDroni")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getDronesList() {
    return Response.ok(DroneList_bean.getinstance()).build();
  }

  // Ultime n statistiche globali (con timestamp) relative alla smart-city
  @Path("ultimeStatistiche/{n}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getUltimeStatistiche(@PathParam("n") int n) {

    StatisticheGlobaliList_bean listaStatistiche = StatisticheGlobaliList_bean.getinstance();

    if (listaStatistiche.getListaStatistiche().size() - n < 0) {

      return Response.status(Response.Status.BAD_REQUEST).build();
    }

    String stringaRisultante = "";

    int i;

    for (i = listaStatistiche.getListaStatistiche().size() - n;
        i < listaStatistiche.getListaStatistiche().size();
        i++) {

      stringaRisultante += "\n" + listaStatistiche.getListaStatistiche().get(i).toString();
    }

    return Response.ok(stringaRisultante).build();
  }

  // Permette di aggiornare le statistiche
  @Path("mediaNumeroConsegne/{t1}/{t2}/{t3}/{t4}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response mediaNumeroConsegne(
      @PathParam("t1") String timeStamp1Parte1,
      @PathParam("t2") String timeStamp1Parte2,
      @PathParam("t3") String timeStamp2Parte1,
      @PathParam("t4") String timeStamp2Parte2) {

    String timeStamp1 = timeStamp1Parte1 + " " + timeStamp1Parte2;
    String timeStamp2 = timeStamp2Parte1 + " " + timeStamp2Parte2;

    Timestamp timeStampInizio = Timestamp.valueOf(timeStamp1);
    Timestamp timeStampFine = Timestamp.valueOf(timeStamp2);

    int mediaNumeroConsegne = 0;
    int numeroConsegne = 0;

    for (StatisticheGlobali_bean statisticheGlobali :
        StatisticheGlobaliList_bean.getinstance().getListaStatistiche()) {

      if (Timestamp.valueOf(statisticheGlobali.getTimestamp()).after(timeStampInizio)
          && Timestamp.valueOf(statisticheGlobali.getTimestamp()).before(timeStampFine)) {

        mediaNumeroConsegne += statisticheGlobali.getMediaNumeroConsegne();
        numeroConsegne += 1;
      }
    }

    mediaNumeroConsegne = mediaNumeroConsegne / numeroConsegne;

    return Response.ok(Integer.toString(mediaNumeroConsegne)).build();
  }

  // Permette di aggiornare le statistiche
  @Path("mediaChilometriPercorsi/{t1}/{t2}/{t3}/{t4}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response mediaChilometriPercorsi(
      @PathParam("t1") String timeStamp1Parte1,
      @PathParam("t2") String timeStamp1Parte2,
      @PathParam("t3") String timeStamp2Parte1,
      @PathParam("t4") String timeStamp2Parte2) {

    String timeStamp1 = timeStamp1Parte1 + " " + timeStamp1Parte2;
    String timeStamp2 = timeStamp2Parte1 + " " + timeStamp2Parte2;

    Timestamp timeStampInizio = Timestamp.valueOf(timeStamp1);
    Timestamp timeStampFine = Timestamp.valueOf(timeStamp2);

    int mediaChilometriPercorsi = 0;
    int numeroConsegne = 0;

    for (StatisticheGlobali_bean statisticheGlobali :
        StatisticheGlobaliList_bean.getinstance().getListaStatistiche()) {

      if (Timestamp.valueOf(statisticheGlobali.getTimestamp()).after(timeStampInizio)
          && Timestamp.valueOf(statisticheGlobali.getTimestamp()).before(timeStampFine)) {

        mediaChilometriPercorsi += statisticheGlobali.getMediaChilometriEffettuati();
        numeroConsegne += 1;
      }
    }

    mediaChilometriPercorsi = mediaChilometriPercorsi / numeroConsegne;

    return Response.ok(Integer.toString(mediaChilometriPercorsi)).build();
  }
}
