package ServerAmministratore.services;

import beans.DroneList_bean;
import beans.Drone_bean;
import beans.StatisticheGlobaliList_bean;
import beans.StatisticheGlobali_bean;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("gestioneDroni")
public class GestioneDroni {

  // Permette di ricevere la lista di Droni registrati
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getDronesList() {
    return Response.ok(DroneList_bean.getinstance()).build();
  }

  // Permette di aggiungere un Drone
  // Esempio Input:
  // {"id":106,"portaComunicazioneDroni":8169,"indirizzoIP":"localhost","posizioneDroneCoordinataX":-1,"posizioneDroneCoordinataY":-1}
  @Path("aggiungiDrone")
  @POST
  @Consumes({"application/json", "application/xml"})
  public Response addDrone(Drone_bean drone) {

    Boolean risultatoOperazioneAggiunta = DroneList_bean.getinstance().add(drone);

    if (risultatoOperazioneAggiunta) {

      DroneList_bean.getinstance().getbyid(drone.getid()).setPosizionePartenzaDrone();

      System.out.print(
          "\nÈ stato aggiunto un drone!\n\nLista droni:" + DroneList_bean.getinstance().toString());
      return Response.ok().build();
    } else {
      return Response.status(Response.Status.CONFLICT).build();
    }
  }

  // Permette di rimuovere un Drone
  // Esempio Input: {"id":52,"portaComunicazioneDroni":23,"indirizzoIP":"123"}
  @Path("rimuoviDrone/{id}")
  @DELETE
  @Consumes({"application/json", "application/xml"})
  public Response deleteDrone(@PathParam("id") String id) {

    Boolean risultatoOperazioneEliminazione;

    risultatoOperazioneEliminazione = DroneList_bean.getinstance().removebyid(Integer.parseInt(id));

    if (risultatoOperazioneEliminazione) {

      System.out.println(
          "\nÈ stato eliminato un drone!\n\nLista droni: "
              + DroneList_bean.getinstance().toString());
      return Response.ok().build();
    } else {

      System.out.println("ID del drone non presente nella lista.");
      return Response.status(Response.Status.NOT_FOUND).build();
    }
  }

  // Permette di prelevare un drone con un determinato ID
  @Path("getDrone/{id}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getByID(@PathParam("id") String id) {
    Drone_bean drone = DroneList_bean.getinstance().getbyid(Integer.parseInt(id));
    // System.out.print("Informazioni Drone: \n" + drone.tostring());
    if (drone != null) {

      return Response.ok(drone).build();
    } else return Response.status(Response.Status.NOT_FOUND).build();
  }

  // Permette di aggiornare le statistiche
  @Path("aggiungiStatistiche")
  @POST
  @Consumes({"application/json", "application/xml"})
  public Response addStatistics(StatisticheGlobali_bean statisticheGlobali) {
    StatisticheGlobaliList_bean.getinstance().getListaStatistiche().add(statisticheGlobali);
    System.out.println("\nSono state aggiunte delle statistiche globali!");
    return Response.ok().build();
  }
}
