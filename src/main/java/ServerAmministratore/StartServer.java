/*

TO-DO

Implementare rimozione di un drone

Gestire sincornizzazione: ingresso e uscita concorrente di 2 o più nodi

Dare possibilità di creare due droni con lo stesso ID a scopo di testing.

Il drone che non riesce ad entrare può riprovare con un ID diverso o abortire.

Utilizziamo una copia delle liste per sincronizzare a grana più fine possibile: ad esempio non dobbiamo bloccare tutta la struttura dati mentre calcoliamo le statistiche in questo modo.

Testare ingresso di due droni: mettere una sleep nell'inserimento della lista per testare.

 */

package ServerAmministratore;

import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;
import java.io.IOException;

public class StartServer {

  private static final String HOST = "localhost";
  private static final int PORT = 1337;

  public static void main(String[] args) throws IOException {
    HttpServer server = HttpServerFactory.create("http://" + HOST + ":" + PORT + "/");
    server.start();

    System.out.println("Server Amministratore in avvio!");
    System.out.println(
        "Server Amministratore avviato, disponibile al seguente indirizzo: http://"
            + HOST
            + ":"
            + PORT);

    System.out.println("Premi return per interromperlo...");
    System.in.read();
    System.out.println("Spegnimento del Server Amministratore in corso.");
    server.stop(0);
    System.out.println("Server Amministratore spento.");
  }
}
