Server REST che riceve statistiche dai droni e che permette la gestione dinamica della rete di droni.

Il Server Amministratore si occupa di mantenere una rappresentazione interna della
smart-city e di ricevere dal drone master le statistiche globali.

Tale server deve quindi offrire due diverse interfacce REST per:

- Gestire la rete di droni e ricevere le statistiche.
- Permettere agli amministratori di effettuare interrogazioni.

## INTERFACCIA DRONI

### INSERIMENTO

Quando vuole inserirsi nel sistema, un drone deve comunicare al server amministratore:
- ID
- Numero di porta sul quale è disponibile per comunicare con gli altri droni
- Indirizzo IP

Al momento della registrazione all’interno del sistema, un drone verrà posizionato in una cella della smart-city, scelta casualmente.
È possibile aggiungere un drone nella rete solo se non esiste un altro drone con lo stesso identificatore.
Se l’inserimento va a buon fine, il server amministratore restituisce al drone la lista di droni già presenti nella smart-city, specificando per ognuno ID, indirizzo IP e numero di porta per la comunicazione.

### RIMOZIONE

Quando un drone si accorge che il proprio livello di batteria è sceso al di sotto del 15 % deve chiedere al server amministratore di uscire dalla rete.

Quando il server amministratore riceve una richiesta di rimozione da parte di un drone deve rimuoverlo dalla smart-city.
È importante notare che il server amministratore si dovrà solo occupare di rimuovere il drone dalla propria struttura dati che rappresenta la smart-city.
Gli altri droni dovranno accorgersi autonomamente dell’uscita di un drone e, di conseguenza, essere in grado di ricostruire l’anello.

### STATISTICHE

Il server amministratore deve fornire dei metodi per ottenere le seguenti informazioni:

- L’elenco dei droni presenti nella rete.
- Ultime n statistiche globali (con timestamp) relative alla smart-city.
- Media del numero di consegne effettuate dai droni della smart-city tra due timestamp t1 e t2.
- Media dei chilometri percorsi dai droni della smart-city tra due timestamp t1 e t2.

N.B. Salvataggio delle statistiche va bene su una struttura dati in locale.