package beans;

import java.util.ArrayList;

public class StatisticheGlobaliList_bean {

  private static StatisticheGlobaliList_bean instance = new StatisticheGlobaliList_bean();

  private ArrayList<StatisticheGlobali_bean> listaStatistiche = new ArrayList<>();

  public static synchronized StatisticheGlobaliList_bean getinstance() {
    if (instance == null) {
      instance = new StatisticheGlobaliList_bean();
    }
    return instance;
  }

  public ArrayList<StatisticheGlobali_bean> getListaStatistiche() {
    return listaStatistiche;
  }
}
