package beans;

import java.util.Random;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Drone_bean {

  private int id;
  private int portaComunicazioneDroni;
  private String indirizzoIP;
  private int posizioneDroneCoordinataX;
  private int posizioneDroneCoordinataY;
  private int livelloBatteria = 100;
  private int idOrdineInGestione =
      -1; // Se è -1 allora non è assegnato. Se è 0 allora il drone ha avviato la procedura di
  // spegnimento.

  public Drone_bean() {};

  public Drone_bean(
      int id,
      int portaComunicazioneDroni,
      String indirizzoIPDrone,
      int posizioneDroneCoordinataX,
      int posizioneDroneCoordinataY) {

    this.id = id;
    this.portaComunicazioneDroni = portaComunicazioneDroni;
    this.indirizzoIP = indirizzoIPDrone;
    this.posizioneDroneCoordinataX = posizioneDroneCoordinataX;
    this.posizioneDroneCoordinataY = posizioneDroneCoordinataY;
  }

  public int getid() {
    return id;
  }

  public int getPortaComunicazioneDroni() {
    return portaComunicazioneDroni;
  }

  public String getIndirizzoIP() {
    return indirizzoIP;
  }

  public int getPosizioneDroneCoordinataX() {
    return posizioneDroneCoordinataX;
  }

  public int getPosizioneDroneCoordinataY() {
    return posizioneDroneCoordinataY;
  }

  public int getLivelloBatteria() {
    return livelloBatteria;
  }

  public int getIdOrdineInGestione() {
    return idOrdineInGestione;
  }

  public void setid(int id) {
    this.id = id;
  }

  public void setPortaComunicazioneDroni(int portaComunicazioneDroni) {
    this.portaComunicazioneDroni = portaComunicazioneDroni;
  }

  public void setIndirizzoIP(String indirizzoIP) {
    this.indirizzoIP = indirizzoIP;
  }

  public void setPosizionePartenzaDrone() {
    Random rand = new Random();
    posizioneDroneCoordinataX = rand.nextInt(10);
    posizioneDroneCoordinataY = rand.nextInt(10);
  }

  public void setPosizioneDroneCoordinataX(int posizioneDroneCoordinataX) {
    this.posizioneDroneCoordinataX = posizioneDroneCoordinataX;
  }

  public void setPosizioneDroneCoordinataY(int posizioneDroneCoordinataY) {
    this.posizioneDroneCoordinataY = posizioneDroneCoordinataY;
  }

  public void setLivelloBatteria(int livelloBatteria) {
    this.livelloBatteria = livelloBatteria;
  }

  public void setIdOrdineInGestione(int idOrdineInGestione) {
    this.idOrdineInGestione = idOrdineInGestione;
  }

  public String toString() { // Cambiato
    return "ID Drone: "
        + id
        + " | Porta Comunicazione Droni: "
        + portaComunicazioneDroni
        + " | IndirizzoIP: "
        + indirizzoIP
        + " | Posizione Drone Coordinata X: "
        + posizioneDroneCoordinataX
        + " | Posizione Drone Coordinata Y: "
        + posizioneDroneCoordinataY
        + "\n";
  }
}
