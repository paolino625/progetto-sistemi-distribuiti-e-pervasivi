package beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class StatisticheGlobali_bean {
  private int mediaNumeroConsegne;
  private int mediaLivelloBatteriaDroni;
  private int mediaChilometriEffettuati; // Ultima consegna
  private int mediaLivelloInquinamento;
  private String timestamp;

  private static StatisticheGlobali_bean instance;

  public StatisticheGlobali_bean() {}

  public StatisticheGlobali_bean(
      int mediaNumeroConsegne,
      int mediaLivelloBatteriaDroni,
      int mediaChilometriEffettuati,
      int mediaLivelloInquinamento,
      String timestamp) {
    this.mediaNumeroConsegne = mediaNumeroConsegne;
    this.mediaLivelloBatteriaDroni = mediaLivelloBatteriaDroni;
    this.mediaChilometriEffettuati = mediaChilometriEffettuati;
    this.mediaLivelloInquinamento = mediaLivelloInquinamento;
    this.timestamp = timestamp;
  }

  public int getMediaNumeroConsegne() {
    return mediaNumeroConsegne;
  }

  public int getMediaLivelloBatteriaDroni() {
    return mediaLivelloBatteriaDroni;
  }

  public int getMediaChilometriEffettuati() {
    return mediaChilometriEffettuati;
  }

  public int getMediaLivelloInquinamento() {
    return mediaLivelloInquinamento;
  }

  public String getTimestamp() {
    return timestamp;
  }

  public void setMediaNumeroConsegne(int mediaNumeroConsegne) {
    this.mediaNumeroConsegne = mediaNumeroConsegne;
  }

  public void setMediaLivelloBatteriaDroni(int mediaLivelloBatteriaDroni) {
    this.mediaLivelloBatteriaDroni = mediaLivelloBatteriaDroni;
  }

  public void setMediaChilometriEffettuati(int mediaChilometriEffettuati) {
    this.mediaChilometriEffettuati = mediaChilometriEffettuati;
  }

  public void setMediaLivelloInquinamento(int mediaLivelloInquinamento) {
    this.mediaLivelloInquinamento = mediaLivelloInquinamento;
  }

  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public String toString() {
    return "StatisticheGlobali:"
        + " mediaNumeroConsegne = "
        + mediaNumeroConsegne
        + ", mediaLivelloBatteriaDroni = "
        + mediaLivelloBatteriaDroni
        + ", mediaChilometriEffettuati = "
        + mediaChilometriEffettuati
        + ", mediaLivelloInquinamento = "
        + mediaLivelloInquinamento
        + '}';
  }
}
