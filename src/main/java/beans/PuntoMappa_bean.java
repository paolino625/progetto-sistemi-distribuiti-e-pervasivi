package beans;

import java.util.Random;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PuntoMappa_bean {

  // Coordinate cartesiane di una cella della griglia rappresentante la smart-city 10x10.
  // I valori vanno quindi da 0 a 9.
  // Ogni cella rappresenta 1 km.

  private int coordinataAsseX = 0;
  private int coordinataAsseY = 0;

  // Non togliere! Necessario per Jersey!
  public int getCoordinataAsseX() {
    return coordinataAsseX;
  }

  // Non togliere! Necessario per Jersey!
  public int getCoordinataAsseY() {
    return coordinataAsseY;
  }

  public void setCoordinataAsseX(int coordinataAsseX) {
    this.coordinataAsseX = coordinataAsseX;
  }

  public void setCoordinataAsseY(int coordinataAsseY) {
    this.coordinataAsseY = coordinataAsseY;
  }

  public PuntoMappa_bean(int coordinataAsseX, int coordinataAsseY) {
    this.coordinataAsseX = coordinataAsseX;
    this.coordinataAsseY = coordinataAsseY;
  }

  public PuntoMappa_bean() {
    Random rand = new Random();

    coordinataAsseX = rand.nextInt(10);
    coordinataAsseY = rand.nextInt(10);
  }

  public String toString() { // Cambiato
    return "Coordinata Asse X: " + coordinataAsseX + "\n" + "Coordinata Asse Y: " + coordinataAsseY;
  }
}
