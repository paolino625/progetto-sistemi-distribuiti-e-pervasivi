/*

Ogni classe che gestisce una risorsa (annotata un xPath) viene istanziata ogni volta che una singola richiesta HTTP viene eseguita. È dunque necessario gestire correttamente l'accesso alla memoria condivisa.
Utilizzando singleton ad esempio o campi statici.
Sincronizzare sull'istanza è inutile.
 */

package beans;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DroneList_bean {

  @XmlElement(name = "droneList")
  private List<Drone_bean> dronesList;

  private static DroneList_bean instance;

  private DroneList_bean() {
    dronesList = new ArrayList<Drone_bean>();
  }

  // Singleton

  public static synchronized DroneList_bean getinstance() {
    if (instance == null) {
      instance = new DroneList_bean();
    }
    return instance;
  }

  public synchronized List<Drone_bean> getArrayListDrones() {

    return new ArrayList<>(dronesList);
  }

  public synchronized void setdroneslist(List<Drone_bean> dronesList) {
    this.dronesList = dronesList;
  }

  public synchronized boolean add(Drone_bean drone) {
    if (getbyid(drone.getid()) == null) {
      dronesList.add(drone);
      return true;
    } else {
      return false;
    }
  }

  public synchronized boolean removebyid(int id) {

    if (getbyid(id) != null) {
      dronesList.remove(getbyid(id));
      return true;
    } else {
      return false;
    }
  }

  public synchronized Drone_bean getbyid(int id) {
    List<Drone_bean> droneListCopy = getArrayListDrones();
    for (Drone_bean drone : droneListCopy) {
      if (drone.getid() == id) {
        return drone;
      }
    }

    return null;
  }

  public String toString() {
    List<Drone_bean> droneListCopy = getArrayListDrones();
    String risultato = "";
    for (Drone_bean drone : droneListCopy) {
      risultato +=
          "\nID Drone: "
              + drone.getid()
              + " | Indirizzo IP: "
              + drone.getIndirizzoIP()
              + " | Numero porta: "
              + drone.getPortaComunicazioneDroni();
    }
    risultato += "\n";
    return risultato;
  }

  public int countdrones() {
    List<Drone_bean> droneListCopy = getArrayListDrones();
    return droneListCopy.size();
  }

  public synchronized void ordina() {
    this.dronesList.sort(Comparator.comparing(Drone_bean::getid));
  }

  public Drone_bean getDroneSuccessivo(int id, int offset) {
    int indiceDrone = dronesList.indexOf(getbyid(id));

    return dronesList.get((indiceDrone + 1 + offset) % dronesList.size());
  }
}
