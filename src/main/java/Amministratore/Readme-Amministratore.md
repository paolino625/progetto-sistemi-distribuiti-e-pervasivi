Client che interroga il Server Amministratore per ottenere informazioni sulle statistiche relative ai droni e alle consegne.

L’applicazione amministratore è un’interfaccia a linea di comando che si occupa di interagire con l’interfaccia REST fornita dal server amministratore.
L’applicazione deve quindi mostrare all’amministratore un semplice menu per scegliere uno dei servizi per amministratori offerti dal server con la possibilità di inserire eventuali parametri richiesti.