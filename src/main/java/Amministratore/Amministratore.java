package Amministratore;

import static Drone.DroneMain.getRequest;

import beans.DroneList_bean;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import java.sql.Timestamp;
import java.util.Scanner;

public class Amministratore {

  public static void main(String[] args) {

    String indirizzoServerAmministratore = "http://localhost:1337";

    Client client = Client.create();
    ClientResponse clientResponse = null;
    String getPath;
    String stringaRisultante;

    while (true) {
      System.out.println(
          "\nMENU AMMINISTRATORE \n1. Elenco dei droni. \n2. Ultime n statistiche globali.\n3. Media del numero di consegne effettuate tra due timestamp.\n4. Media dei chilometri percorsi dai droni della smart-city tra due timestamp.\n5. Esci dal programma.\n");
      System.out.print("Inserisci il numero relativo all'opzione desiderata: ");
      Scanner scanner = new Scanner(System.in);
      String primoTimeStamp;
      String secondoTimeStamp;
      Timestamp dummy;
      String[] arrayStringhe;

      int opzioneScelta = scanner.nextInt();
      switch (opzioneScelta) {

          //  L’elenco dei droni presenti nella rete
        case 1:
          getPath = "/utente/listaDroni";
          clientResponse = getRequest(client, indirizzoServerAmministratore + getPath);
          System.out.println("\n" + clientResponse.toString());
          DroneList_bean droneListBean = clientResponse.getEntity(DroneList_bean.class);
          System.out.println(droneListBean.toString());

          break;

          // Ultime n statistiche globali (con timestamp) relative alla smart-city
        case 2:
          System.out.print("Inserisci il numero delle statistiche più recenti che vuoi vedere: ");
          int numeroStatistiche = scanner.nextInt();

          getPath = "/utente/ultimeStatistiche/";
          clientResponse =
              getRequest(client, indirizzoServerAmministratore + getPath + numeroStatistiche);
          System.out.println("\n" + clientResponse.toString());
          stringaRisultante = clientResponse.getEntity(String.class);
          System.out.println(stringaRisultante);

          break;

          // Media del numero di consegne effettuate dai droni della smart-city tra due timestamp t1
          // e t2
        case 3:
          dummy = new Timestamp(System.currentTimeMillis());
          System.out.println("Inserisci il primo TimeStamp: ");
          System.out.println("Formato: " + dummy.toString());
          scanner.nextLine();
          arrayStringhe = scanner.nextLine().split(" ");
          primoTimeStamp = arrayStringhe[0] + "/" + arrayStringhe[1];

          System.out.println("Inserisci il secondo TimeStamp: ");
          System.out.println("Formato: " + dummy.toString());
          arrayStringhe = scanner.nextLine().split(" ");
          secondoTimeStamp = arrayStringhe[0] + "/" + arrayStringhe[1];

          getPath = "/utente/mediaNumeroConsegne/";
          clientResponse =
              getRequest(
                  client,
                  indirizzoServerAmministratore
                      + getPath
                      + primoTimeStamp
                      + "/"
                      + secondoTimeStamp);
          System.out.println("\n" + clientResponse.toString());
          stringaRisultante = clientResponse.getEntity(String.class);
          System.out.println(stringaRisultante);

          break;

          // Media dei chilometri percorsi dai droni della smart-city tra due timestamp t1 e t2
        case 4:
          dummy = new Timestamp(System.currentTimeMillis());
          System.out.println("Inserisci il primo TimeStamp: ");
          System.out.println("Formato: " + dummy.toString());
          scanner.nextLine();
          arrayStringhe = scanner.nextLine().split(" ");
          primoTimeStamp = arrayStringhe[0] + "/" + arrayStringhe[1];

          System.out.println("Inserisci il secondo TimeStamp: ");
          System.out.println("Formato: " + dummy.toString());
          arrayStringhe = scanner.nextLine().split(" ");
          secondoTimeStamp = arrayStringhe[0] + "/" + arrayStringhe[1];

          getPath = "/utente/mediaChilometriPercorsi/";
          clientResponse =
              getRequest(
                  client,
                  indirizzoServerAmministratore
                      + getPath
                      + primoTimeStamp
                      + "/"
                      + secondoTimeStamp);
          System.out.println("\n" + clientResponse.toString());
          stringaRisultante = clientResponse.getEntity(String.class);
          System.out.println(stringaRisultante);

          break;

          // Input non corretto
        case 5:
          System.exit(0);
          break;

        default:
          System.out.println("Numero non corretto: riprova.");
      }
    }
  }
}
