package ClassiUtili;

import beans.PuntoMappa_bean;

public class Ordine {

  private int ordineID;
  private PuntoMappa_bean puntoRitiro, puntoConsegna;
  private int droneAssegnato = 0;

  public Ordine(int ordineID, PuntoMappa_bean puntoRitiro, PuntoMappa_bean puntoConsegna) {
    this.ordineID = ordineID;
    this.puntoRitiro = puntoRitiro;
    this.puntoConsegna = puntoConsegna;
  }

  @Override
  public String toString() {
    return "Ordine ID: "
        + ordineID
        + "\nPunto ritiro: \n"
        + puntoRitiro.toString()
        + "\nPunto consegna: \n"
        + puntoConsegna.toString()
        + "\nDrone assegnato: "
        + droneAssegnato
        + "\n";
  }

  public int getOrdineID() {
    return ordineID;
  }

  public PuntoMappa_bean getPuntoRitiro() {
    return puntoRitiro;
  }

  public PuntoMappa_bean getPuntoConsegna() {
    return puntoConsegna;
  }

  public int getDroneAssegnato() {
    return droneAssegnato;
  }

  public void setDroneAssegnato(int droneAssegnato) {
    this.droneAssegnato = droneAssegnato;
  }
}
