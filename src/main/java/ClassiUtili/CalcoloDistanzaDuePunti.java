package ClassiUtili;

import beans.PuntoMappa_bean;

public class CalcoloDistanzaDuePunti {

  public static double effettuaCalcolo(PuntoMappa_bean punto1, PuntoMappa_bean punto2) {

    return Math.sqrt(
        Math.pow((punto2.getCoordinataAsseX() - punto1.getCoordinataAsseX()), 2)
            + (Math.pow((punto2.getCoordinataAsseY() - punto1.getCoordinataAsseY()), 2)));
  }
}
