Applicativo che simula un sito di e-commerce, generando periodicamente nuove consegne da dover effettuare e comunicando
le informazioni relative ad esse allo stormo di droni.

Tale processo deve generare un nuovo ordine per cui è richiesta una consegna ogni 5 secondi.

Ogni ordine è caratterizzato da:

- ID
- Punto di ritiro
- Punto di consegna

Gli ordini generati da Dronazon, devono essere comunicati tramite il protocollo MQTT visto a lezione.
Per semplicità, si assume che il broker MQTT della smart-city sia attivo al seguente indirizzo tcp://localhost:1883.
Dronazon deve connettersi a tale broker come client MQTT e assumerà il ruolo di publisher.
Dronazon pubblica tale aggiornamento sul seguente topic MQTT: dronazon/smartcity/orders/.