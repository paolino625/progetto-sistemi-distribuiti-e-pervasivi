/*

Drone Master è in attesa degli ordini per poi assegnarli ai Droni sulla rete.
Potenzialmente ogni drone potrebbe diventare un drone master, quindi l'ascolto dei messaggi di Drona Zone deve essere disponibile su tutti i droni, nel caso in cui diventino master devono essere pronti.


 */

package DronaZone;

import ClassiUtili.Ordine;
import beans.PuntoMappa_bean;
import com.google.gson.Gson;
import java.util.Random;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class DronaZone {
  public static void main(String[] args) throws InterruptedException {
    MqttClient client;
    String broker = "tcp://localhost:1883";
    String clientId = MqttClient.generateClientId();
    String topic = "dronazon/smartcity/orders/";
    int qos = 2;

    // Mosquitto start

    while (true) {

      try {
        client = new MqttClient(broker, clientId);
        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true);
        // connOpts.setUserName(username); // opzionale
        // connOpts.setPassword(password.toCharArray()); // opzionale
        // connOpts.setWill("this/is/a/topic","will message".getBytes(),1,false);  // opzionale
        // connOpts.setKeepAliveInterval(60);  // opzionale

        // Connessione al client
        System.out.println("Connessione con il Broker " + broker);
        client.connect(connOpts);
        System.out.println("Connesso");

        // String payload = String.valueOf(0 + (Math.random() * 10)); // Crea un numero random tra 0
        // e 10
        // MqttMessage message = new MqttMessage(payload.getBytes());

        Ordine ordine = creoOrdineCasuale();

        Gson gson = new Gson();
        String payloadJson = gson.toJson(ordine);

        MqttMessage message = new MqttMessage(payloadJson.getBytes());

        // Set QoS del messaggio
        message.setQos(qos);
        System.out.println("Messaggio in pubblicazione: " + payloadJson + " ...");
        client.publish(topic, message);
        System.out.println("Messaggio pubblicato.");

        if (client.isConnected()) client.disconnect();
        System.out.println("Publisher " + clientId + " disconnesso!\n");

      } catch (MqttException me) {
        System.out.println("reason " + me.getReasonCode());
        System.out.println("msg " + me.getMessage());
        System.out.println("loc " + me.getLocalizedMessage());
        System.out.println("cause " + me.getCause());
        System.out.println("excep " + me);
        me.printStackTrace();
      }

      Thread.sleep(5000); // Aspetto 10 secondi prima di inviare il prossimo messaggio
    }
  }

  private static Ordine creoOrdineCasuale() {

    Random rand = new Random();

    int ordineID = rand.nextInt(1500);
    PuntoMappa_bean puntoRitiro = new PuntoMappa_bean();
    PuntoMappa_bean puntoConsegna = new PuntoMappa_bean();

    Ordine ordine = new Ordine(ordineID, puntoRitiro, puntoConsegna);
    return ordine;
  }
}
