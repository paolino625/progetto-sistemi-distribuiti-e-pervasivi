# INTRODUZIONE DRONE

Ogni drone è simulato da un processo (non come thread) che si occupa di:

- Coordinarsi con gli altri droni tramite gRPC per:
  -- Eleggere il drone master della smart-city tramite l’algoritmo di elezione ring-based visto durante il corso di teoria
  -- Inviare statistiche al server amministratore

- Effettuare le consegne assegnate dal drone master.

- Uscire dal sistema quando il proprio livello di batteria si trova al di sotto del 15%.

# INIZIALIZZAZIONE DRONE

Un drone deve essere inizializzato specificando:

- ID.
- Porta di ascolto per la comunicazione tra droni.
- Indirizzo del server amministratore.

Una volta avviato, il processo drone deve registrarsi al sistema tramite il server amministratore.
Se l’inserimento andrà a buon fine il drone riceve dal server:

- La propria posizione di partenza nella smart-city
- L’elenco di droni già presenti nella smart-city

# STRUTTURA DELLA RETE

La rete di droni ha una struttura ad anello.
La creazione di tale rete deve essere gestita in modo decentralizzato, senza essere guidata dal server amministratore.
!! È quindi necessario tenere in considerazione i possibili problemi di sincronizzazione distribuita che possono avvenire in ingresso ed uscita dall’anello.

Ricevute queste informazioni, il drone deve quindi inserirsi nella rete ad anello in maniera decentralizzata, senza l’aiuto del server amministratore.
Nel caso in cui non ci siano altri droni all’interno della smart-city, il drone si auto-proclama master.
Altrimenti, il drone deve presentarsi agli altri droni della smart-city inviando la propria posizione nella griglia e capire chi sia il drone master.
Durante questa fase di presentazione il drone non necessita di comunicare il livello della batteria in quanto si assume che sia al 100 %.

Quando l‘inserimento del drone nella smart-city va a buon ﬁne, esso dovrà avviare il proprio sensore per il rilevamento dell’inquinamento dell’aria.

La struttura deve rimanere consistente anche quando un drone o più droni cercano di entrare o lasciare la rete, ci sono diversi casi limite.

## Note

Uno dei problemi è che i riferimenti di un drone ai droni precedenti/successivi dell'anello può essere aggiornato in maniera concorrente.
La consistena deve essere mantenuta quando è utile: non deve essere mantenuta in ogni istante temporale.

Si possono testare i casi limite usando il metodo sleep.

## ELEZIONE

Quando un drone si accorge dell'assenza del master, esso deve avviare l’elezione di un nuovo drone master tramite l‘algoritmo ring-based visto a lezione (Chang and Roberts).
Viene eletto il drone con livello di batteria maggiore.
In caso di parità quello con ID maggiore.

Durante l'elezione è obbligatorio sfruttare la rete ad anello per le comunicazioni.

Si noti che se un drone sta effettuando una consegna durante un'elezione, bisogna considerare come livello di batteria quello che avrà al termine della consegna.

Non appena viene eletto un nuovo drone master, tutti gli altri droni dovranno comunicargli la propria posizione all’interno della smart-city ed il proprio livello di batteria.

# CONSUMO DI BATTERIA

Al termine di ogni consegna un drone perderà un percentuale di batteria pari al 10%.
Si noti che per semplicità si assume che la percentuale di batteria consumata per una consegna venga calcolata solo al temine di quest’ultima.

# CHIUSURA ESPLICITA

Si assume che ogni drone possa terminare solamente in maniera controllata.

- In particolare, ogni drone chiede al server amministratore di uscire dal sistema non appena il suo livello di batteria è inferiore al 15%.
- Inoltre, un drone può richiedere autonomamente di uscire dalla rete tramite una richiesta esplicita (e.g., quit) da linea di comando.

Passi che un drone deve effettuare dalla rete:

## DRONE NON MASTER

- Terminare l’eventuale consegna di cui si sta occupando, comunicando poi al drone master le informazioni descritte nella Sezione 5.6.2.
- Chiudere forzatamente le comunicazioni con gli altri droni, senza curarsi di comunicare loro la propria uscita dalla rete.
- Chiedere al server amministratore di uscire dalla rete.

## DRONE MASTER

- Terminare l’eventuale consegna di cui si sta occupando, memorizzando le informazioni descritte nella Sezione 5.6.2. (Può aspettare la risposta delle ultime consegne che ha assegnato)
- Disconnettersi dal broker MQTT della smart-city.
- Assicurarsi di assegnare le consegne pendenti ai droni della smart-city.
- Chiudere forzatamente le comunicazioni con gli altri droni, senza curarsi di comunicare loro la propria uscita dalla rete.
- Inviare al server amministratore le statistiche globali della smart-city.
- Chiedere al server amministratore di uscire dal sistema.

# SENSORE INQUINAMENTO

- Ogni sensore di inquinamento produce periodicamente delle misurazioni relative al livello di polveri sottili nell’aria (PM10).

Ogni singola misurazione è caratterizzata da:
- Valore di PM10 letto
- Timestamp in millisecondi

La generazione di queste misurazioni viene svolta da opportuni simulatori, il cui codice viene fornito per semplificare lo sviluppo del progetto.
Ogni simulatore è un thread che consiste in un loop infinito che simula periodicamente (con frequenza predefinita) le misurazioni, aggiungendole ad una struttura dati.

Di questa struttura dati viene fornita solo l'interfaccia (Buffer) la quale espone due metodi:
- void add(Measurement m)
- List <Measurement> readAllAndClean()

Ogni drone avrà un singolo sensore e quindi un solo buffer.

Il metodo readAllAndClean deve essere usato per ottenere tutte le misurazioni contenute nella struttura dati. 
Al termine di una lettura, readAllAndClean deve occuparsi di svuotare il buffer in modo da fare spazio a nuove misurazioni. 
In particolare, i dati dei sensori devono essere processati usando la tecnica della sliding window presentata durante le lezioni di teoria, 
considerando una dimensione del buffer di 8 misurazioni ed un overlap del 50%. 
Quando la dimensione del buffer raggiunge le 8 misurazioni, deve essere effettuata la media dei dati nel buffer. 
Le medie calcolate in questo modo verranno poi trasmesse insieme alle informazioni sulle consegne.

# SINCRONIZZAZIONE DISTRIBUITA

## GESTIONE DELLE CONSEGNE

Ogni qualvolta Dronazon genera una nuova consegna da effettuare, questa viene pubblicata dal sito di e-commerce tramite protocollo MQTT sul topic dronazon/smartcity/orders/

Quando viene eletto un nuovo master, questo, prima di effettuare qualsiasi operazione, deve attendere di ricevere la posizione degli altri droni della smart-city, senza il supporto del server amministratore.
Successivamente, il drone master deve connettersi al broker MQTT della smart-city e registrarsi come subscriber al topic dronazon/smartcity/orders/, per ricevere informazioni sulle nuove consegne.
Quando un drone master riceve una richiesta per una nuova consegna, esso la assegnerà al drone della smart-city (incluso sè stesso) che rispetta i seguenti criteri:

- Il drone non deve essere già impegnato in un’altra consegna.
- Tra i droni che rispettano i criteri precedenti, viene scelto quello più vicino al luogo di ritiro dell’ordine con maggior livello di batteria residuo.
- Nel caso in cui più di un drone rispetti tutti i criteri elencati in precedenza, verrà selezionato per la consegna il drone con ID maggiore.

Per calcolare la distanza tra due punti: d(P1,P2) = Radice((x2-x1)^2 + (y2-y1)^2)

Se non ci sono droni disponibili per effettuare una consegna, questa viene inserita in un’apposita coda gestita dal drone master.
Ogni volta che un drone porta a termine una consegna comunicando le relative statistiche al drone master, quest’ultimo utilizza i criteri precedentemente descritti per assegnare le consegne pendenti che si trovano nella coda.

Quando un drone riceve una consegna da effettuare, il tempo impiegato per consegnare l’ordine viene simulato da una Thread.sleep() di 5 secondi.

## CALCOLO DELLE INFORMAZIONI DEL DRONE

Ogni volta che un drone effettua una consegna deve trasmettere al drone master le seguenti informazioni:

- Timestamp di arrivo al luogo di consegna.
- Nuova posizione del drone che coincide con la posizione di consegna.
- Chilometri percorsi per raggiungere i punti di ritiro e consegna.
- Medie delle misurazioni relative al livello di inquinamento dell’aria rilevate a partire dall’ultima consegna effettuata.
- Il proprio livello di batteria residuo.

Queste informazioni verranno poi utilizzate dal drone master per calcolare le statistiche globali della smart-city. 

Ogni 10 secondi un drone deve stampare a schermo: il numero totale di consegne effettuate, i chilometri percorsi e la percentuale di batteria residua.

## INVIO DELLE INFORMAZIONI AL SERVER

I droni devono coordinarsi per inviare periodicamente al Server Amministratore le statistiche globali della smart-city. 
Sarà il drone master a calcolare le statistiche globali ogni 10 secondi (la media delle medie dei droni), considerando la media delle informazioni ricevute dai singoli droni al momento delle loro consegne.

Nello specifico le statistiche globali sono le seguenti:

- Media del numero di consegne effettuate dai droni.
- Media dei chilometri percorsi dai droni (nell'ultima consegna, non totali).
- Media del livello di inquinamento rilevato dai droni.
- Media del livello di batteria residuo dei droni.

Quando il drone master termina di calcolare le statistiche globali, esso le invia al server amministratore insieme al timestamp in cui le statistiche sono state calcolate.
Per timestamp si intende il timestamp relativi al calcolo delle statistiche, i timestamp dei droni sono scartabili.

N.B. Occhio alla sincronizzazione di calcolo e lettura di statistiche.

# BATTERIA

- Il sistema deve essere esteso per dar la possibilità ai droni di ricaricare il proprio livello di batteria.
  Tale operazione potrà essere svolta da un solo drone alla volta.
  È possibile richiedere di ricaricare il livello di batteria di un drone tramite richiesta esplicita da linea di comando (e.g., recharge). 
  Quando si verifica tale evento, il drone deve richiedere agli altri droni di poter ricaricare la propria batteria, tramite l’algoritmo Ricart and Agrawala visto durante il corso di teoria. 

- Quando ad un drone viene concessa la possibilità di ricaricarsi, questo simulerà il processo di ricarica con una Thread.sleep() di 10 secondi. 
  Durante questa fase, il drone non può ricevere consegne da effettuare. 
  Terminata la fase di ricarica, il drone comunica al master della smart-city:
  -- La sua nuova posizione all’interno della smart-city, ovvero (0,0). 
  -- Il suo nuovo livello di batteria residua, ovvero 100%