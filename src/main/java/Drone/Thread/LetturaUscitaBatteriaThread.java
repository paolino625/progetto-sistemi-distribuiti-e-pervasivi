package Drone.Thread;

import Drone.DroneMain;

public class LetturaUscitaBatteriaThread implements Runnable {

  public LetturaUscitaBatteriaThread(DroneMain drone) {
    this.drone = drone;
  }

  DroneMain drone;

  @Override
  public void run() {
    while (true) {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      if (drone.getLivelloBatteria() < 15 && !drone.ricaricaBatteriaInCorso) {
        drone.esciDallaRete();
      }
    }
  }
}
