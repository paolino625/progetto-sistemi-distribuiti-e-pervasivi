package Drone.Thread;

import ClassiUtili.Ordine;
import Drone.DroneMain;
import com.google.gson.Gson;
import java.sql.Timestamp;
import org.eclipse.paho.client.mqttv3.*;

public class LetturaServerDronaZoneThread implements Runnable {

  private final DroneMain droneMain;

  public LetturaServerDronaZoneThread(DroneMain droneMain) {
    this.droneMain = droneMain;
  }

  @Override
  public void run() {
    while (true) {

      if (droneMain.sonoMaster() && !droneMain.richiestaChiusuraDrone) {

        MqttClient client;
        String broker = "tcp://localhost:1883";
        String clientId = MqttClient.generateClientId();
        String topic = "dronazon/smartcity/orders/";
        int qos = 2;

        try {
          client = new MqttClient(broker, clientId);
          MqttConnectOptions connOpts = new MqttConnectOptions();
          connOpts.setCleanSession(true);

          // Connessione al broker
          System.out.println("\nIn connessione con il Broker " + broker);
          client.connect(connOpts);
          System.out.println(
              "Connesso con il broker. Thread PID: " + Thread.currentThread().getId());

          // Callback
          client.setCallback(
              new MqttCallback() {

                public void messageArrived(String topic, MqttMessage message) {
                  // Chiamato quando un messaggio arriva dal server e matcha qualsiasi
                  // sottoiscrizione effettuata dal client.
                  String time = new Timestamp(System.currentTimeMillis()).toString();
                  String messaggioRicevuto = new String(message.getPayload());
                  System.out.println(
                      "\nHo ricevuto un messaggio! - Callback - Thread PID: "
                          + Thread.currentThread().getId()
                          + "\n\tTimestamp:    "
                          + time
                          + "\n\tTopic:   "
                          + topic
                          + "\n\tMessaggio arrivato: "
                          + messaggioRicevuto
                          + "\n\tQoS:     "
                          + message.getQos()
                          + "\n");

                  // System.out.println("\n ***  Press a random key to exit *** \n");

                  Gson gson = new Gson();
                  Ordine nuovoOrdine = gson.fromJson(messaggioRicevuto, Ordine.class);

                  synchronized (droneMain.listaOrdiniLock) {
                    while (droneMain.listaOrdiniInUtilizzo) {
                      try {

                        droneMain.listaOrdiniLock.wait();

                      } catch (InterruptedException e) {
                        e.printStackTrace();
                      }
                    }
                  }
                  droneMain.listaOrdiniInUtilizzo = true;

                  droneMain.aggiungiOrdine(nuovoOrdine);

                  droneMain.listaOrdiniInUtilizzo = false;
                  synchronized (droneMain.listaOrdiniLock) {
                    droneMain.listaOrdiniLock.notifyAll();
                  }
                }

                public void connectionLost(Throwable cause) {
                  System.out.println(
                      clientId
                          + " Connectionlost! cause:"
                          + cause.getMessage()
                          + "-  Thread PID: "
                          + Thread.currentThread().getId());
                }

                public void deliveryComplete(IMqttDeliveryToken token) {
                  // Non usato qui
                }
              });
          System.out.println(
              "Mi sto iscrivendo... - Thread PID: " + Thread.currentThread().getId());
          client.subscribe(topic, qos);
          System.out.println("Iscritto ai seguenti topic: " + topic);

          while (!droneMain.richiestaChiusuraDrone) {
            try {
              Thread.sleep(2000);
            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

          System.out.println("Client MQTT disconnesso correttamente.");
          client.disconnect();

        } catch (MqttException me) {
          System.out.println("reason " + me.getReasonCode());
          System.out.println("msg " + me.getMessage());
          System.out.println("loc " + me.getLocalizedMessage());
          System.out.println("cause " + me.getCause());
          System.out.println("excep " + me);
          me.printStackTrace();
        }
      }
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
