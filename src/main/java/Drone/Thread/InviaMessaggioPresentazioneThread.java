package Drone.Thread;

import Drone.DroneMain;
import beans.Drone_bean;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.PresentazioneDroneGrpc;

public class InviaMessaggioPresentazioneThread implements Runnable {

  private final DroneMain droneMain;
  private final Drone_bean drone;
  private final Boolean nodoSuccessivo;

  public InviaMessaggioPresentazioneThread(
      DroneMain drone, Drone_bean drone_bean, boolean nodoSuccessivo) {
    droneMain = drone;
    this.drone = drone_bean;
    this.nodoSuccessivo = nodoSuccessivo;
  }

  @Override
  public void run() {

    // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + drone.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    // Creo un blocco stub sul canale
    PresentazioneDroneGrpc.PresentazioneDroneBlockingStub stub =
        PresentazioneDroneGrpc.newBlockingStub(canale);

    DroneNetwork.PresentazioneDroneRequest richiesta;

    if (nodoSuccessivo) {

      System.out.println(
          "Invio messaggio di presentazione al mio drone successivo che è il n° "
              + drone.getid()
              + ".");

      richiesta =
          DroneNetwork.PresentazioneDroneRequest.newBuilder()
              .setIdDrone(droneMain.getId())
              .setIndirizzoDrone(droneMain.getIndirizzoDrone())
              .setPortaDrone(droneMain.getPortaComunicazioneDroni())
              .setCoordinataX(droneMain.getPosizioneAttuale().getCoordinataAsseX())
              .setCoordinataY(droneMain.getPosizioneAttuale().getCoordinataAsseY())
              .setTimestampLogico(droneMain.getTimeStampLogico())
              .setNodoSuccessivo(true)
              .build();

    } else {

      System.out.println(
          "Invio messaggio di presentazione al drone n° "
              + drone.getid()
              + " che non è il mio successivo.");
      richiesta =
          DroneNetwork.PresentazioneDroneRequest.newBuilder()
              .setIdDrone(droneMain.getId())
              .setIndirizzoDrone(droneMain.getIndirizzoDrone())
              .setPortaDrone(droneMain.getPortaComunicazioneDroni())
              .setCoordinataX(droneMain.getPosizioneAttuale().getCoordinataAsseX())
              .setCoordinataY(droneMain.getPosizioneAttuale().getCoordinataAsseY())
              .setTimestampLogico(droneMain.getTimeStampLogico())
              .setNodoSuccessivo(false)
              .build();
    }

    System.out.println("Effettuo richiesta gRPC alla porta: " + drone.getPortaComunicazioneDroni());
    DroneNetwork.PresentazioneDroneResponse risposta = stub.presentazioneDrone(richiesta);

    int idMasterDrone = risposta.getIdDroneMaster();
    if (idMasterDrone >= 0) {
      // Il nodo dal quale ricevo il messaggio è il mio successivo, il quale mi dice qual'è il
      // master nella rete.
      System.out.println(
          "\nHo ricevuto un messaggio dal mio successivo.\nL'ID del drone master è: "
              + idMasterDrone
              + "\n");
      droneMain.setIdMasterDrone(idMasterDrone);
    } else {
      System.out.println("Ho ricevuto un messaggio da un drone che non è il mio successivo.\n");
    }

    // Chiudo il canale
    canale.shutdown();
  }
}
