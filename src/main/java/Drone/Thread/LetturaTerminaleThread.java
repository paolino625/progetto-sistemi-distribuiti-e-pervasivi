package Drone.Thread;

import Drone.DroneMain;
import java.util.Scanner;

public class LetturaTerminaleThread implements Runnable {

  private final DroneMain drone;
  private final Scanner scanner;

  public LetturaTerminaleThread(DroneMain drone, Scanner scanner) {
    this.drone = drone;
    this.scanner = scanner;
  }

  @Override
  public void run() {
    while (true) {
      String stringaLetta = scanner.next();
      if (stringaLetta.equals("q")) {
        if (drone.elezioneInCorso) {
          System.out.println(
              "\nC'è un'elezione in corso!\nAspetto che l'elezione finisca per uscire...");
          synchronized (drone.elezioneLock) {
            while (drone.elezioneInCorso) {
              try {

                drone.elezioneLock.wait();

              } catch (InterruptedException e) {
                e.printStackTrace();
              }
            }
          }

          System.out.println("\nElezione finita. Procedo con l'uscita dalla rete...");
          drone.esciDallaRete();

          synchronized (drone.elezioneLock) {
            drone.elezioneLock.notifyAll();
          }
        } else {
          drone.esciDallaRete();
        }
      }
      if (stringaLetta.equals("r")) {
        drone.tentaRicaricaBatteria();
      }
    }
  }
}
