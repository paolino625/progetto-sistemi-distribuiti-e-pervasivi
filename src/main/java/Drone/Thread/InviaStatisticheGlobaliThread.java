package Drone.Thread;

import Drone.DroneMain;
import beans.StatisticheGlobali_bean;
import java.sql.Timestamp;

public class InviaStatisticheGlobaliThread implements Runnable {
  private DroneMain droneMain;

  public InviaStatisticheGlobaliThread(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void run() {

    while (!droneMain.invioUltimeStatisticheAlServer) {
      // Aspetto 10 secondi
      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      if (droneMain.sonoMaster()) {

        StatisticheGlobali_bean statisticheGlobali = droneMain.getStatisticheGlobaliCopy();
        droneMain.azzeraStatisticheGlobali();

        statisticheGlobali.setMediaLivelloBatteriaDroni(
            droneMain.calcolaMediaLivelloBatteriaDroni());

        if (statisticheGlobali.getMediaNumeroConsegne() != 0) {
          statisticheGlobali.setMediaChilometriEffettuati(
              statisticheGlobali.getMediaChilometriEffettuati()
                  / statisticheGlobali.getMediaNumeroConsegne());
          statisticheGlobali.setMediaLivelloInquinamento(
              statisticheGlobali.getMediaLivelloInquinamento()
                  / statisticheGlobali.getMediaNumeroConsegne());
        }
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        statisticheGlobali.setTimestamp(timestamp.toString());

        System.out.println("\nSto per inviare le statistiche globali al server.");
        System.out.println(statisticheGlobali.toString());

        droneMain.inviaStatisticheAlServer(statisticheGlobali);
      }
    }
  }
}
