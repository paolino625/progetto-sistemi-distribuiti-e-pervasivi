package Drone.Thread;

import ClassiUtili.CalcoloDistanzaDuePunti;
import ClassiUtili.Ordine;
import Drone.DroneMain;
import beans.Drone_bean;
import beans.PuntoMappa_bean;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.OrdineGrpc;

public class AssegnaOrdiniThread implements Runnable {
  private final DroneMain droneMain;

  public AssegnaOrdiniThread(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void run() {
    while (true) {

      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      synchronized (droneMain.listaOrdiniLock) {
        while (droneMain.listaOrdiniInUtilizzo) {
          try {

            droneMain.listaOrdiniLock.wait();
          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
      droneMain.listaOrdiniInUtilizzo = true;

      // Esco nel caso in cui sia stata richiesta la chiusura del drone e non siano rimasti ordini
      // da consegnare.
      if (droneMain.richiestaChiusuraDrone && droneMain.getListaOrdiniCopy().size() < 1) {

        return;
      }

      for (Ordine ordine : droneMain.getListaOrdini()) {

        if (ordine.getDroneAssegnato() == 0) {
          System.out.println("\nOrdine n° " + ordine.getOrdineID() + " ancora non assegnato.");
          System.out.println("\nLista droni: \n" + droneMain.getDroneListBean().toString());
          Drone_bean droneMigliore = null;

          for (Drone_bean drone : droneMain.getDroneListBean().getArrayListDrones()) {
            System.out.println(
                "Drone n° " + drone.getid() + " preso in esame durante assegnamento ordini.");

            if (drone.getIdOrdineInGestione() == -1 && drone.getLivelloBatteria() > 15) {

              System.out.println("Drone n° " + drone.getid() + " è libero.");

              if (droneMigliore
                  == null) { // 1° Giro del ciclo: assegniamo il primo drone alla variabile
                // droneMigliore
                droneMigliore = drone;
              } else { // 2° Giro del ciclo o più
                PuntoMappa_bean posizioneDrone =
                    new PuntoMappa_bean(
                        drone.getPosizioneDroneCoordinataX(), drone.getPosizioneDroneCoordinataY());
                PuntoMappa_bean posizioneDroneMigliore =
                    new PuntoMappa_bean(
                        droneMigliore.getPosizioneDroneCoordinataX(),
                        droneMigliore.getPosizioneDroneCoordinataY());

                // Il droneMigliore ha una distanza minore dal punto di ritiro dell'ordine, quindi
                // non facciamo nulla.
                if (CalcoloDistanzaDuePunti.effettuaCalcolo(ordine.getPuntoRitiro(), posizioneDrone)
                    > CalcoloDistanzaDuePunti.effettuaCalcolo(
                        ordine.getPuntoRitiro(), posizioneDroneMigliore)) {
                  // Non faccio nulla.
                }
                // Il drone preso in esame ha una distanza minore dal punto di ritiro dell'ordine,
                // quindi lo impostiamo come droneMigliore.
                else if (CalcoloDistanzaDuePunti.effettuaCalcolo(
                        ordine.getPuntoRitiro(), posizioneDrone)
                    < CalcoloDistanzaDuePunti.effettuaCalcolo(
                        ordine.getPuntoRitiro(), posizioneDroneMigliore)) {
                  droneMigliore = drone;
                }

                // Il drone preso in esame ha una distanza pari a quella del droneMigliore dal punto
                // di ritiro dell'ordine.
                else {
                  // Il drone ha un livello di batteria maggiore rispetto al drone migliore. Quindi
                  // lo imposto come drone migliore.
                  if (drone.getLivelloBatteria() > droneMigliore.getLivelloBatteria()) {
                    droneMigliore = drone;
                  }
                  // Il drone migliore ha un livello di batteria maggiore. Quindi non faccio nulla.
                  else if (drone.getLivelloBatteria() < droneMigliore.getLivelloBatteria()) {
                    // Non faccio nulla.
                  }
                  // Entrambi i droni hanno il medesimo livello di batteria.
                  else {
                    // L'ID del drone è maggiore di quello del drone migliore, quindi lo imposto
                    // come drone migliore.
                    if (drone.getid() > droneMigliore.getid()) {
                      droneMigliore = drone;
                    } else {
                      // Non faccio nulla.
                    }
                  }
                }
              }

            } else {
              System.out.println(
                  "Drone n° " + drone.getid() + " è occupato o ha la batteria scarica.");
            }
          }

          // Se c'è un drone disponibile.
          if (droneMigliore != null) {

            System.out.println(
                "Il drone migliore per gestire questo ordine è il drone n° "
                    + droneMigliore.getid()
                    + ".");
            ordine.setDroneAssegnato(droneMigliore.getid());
            droneMigliore.setIdOrdineInGestione(ordine.getOrdineID());

            // Invia messaggio gRPC al drone prescelto.

            Drone_bean drone = droneMain.getDroneListBean().getbyid(droneMigliore.getid());

            // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
            final ManagedChannel canale =
                ManagedChannelBuilder.forTarget("localhost:" + drone.getPortaComunicazioneDroni())
                    .usePlaintext()
                    .build();

            // Creo un blocco stub sul canale
            OrdineGrpc.OrdineBlockingStub stub = OrdineGrpc.newBlockingStub(canale);

            DroneNetwork.OrdineRequest richiesta =
                DroneNetwork.OrdineRequest.newBuilder()
                    .setIdDrone(droneMain.getId())
                    .setIdOrdine(ordine.getOrdineID())
                    .setPuntoRitiroCoordinataX(ordine.getPuntoRitiro().getCoordinataAsseX())
                    .setPuntoRitiroCoordinataY(ordine.getPuntoRitiro().getCoordinataAsseY())
                    .setPuntoConsegnaCoordinataX(ordine.getPuntoConsegna().getCoordinataAsseX())
                    .setPuntoConsegnaCoordinataY(ordine.getPuntoConsegna().getCoordinataAsseY())
                    .setTimestampLogico(droneMain.getTimeStampLogico())
                    .build();

            System.out.println(
                "Effettuo richiesta gRPC alla porta: " + drone.getPortaComunicazioneDroni());

            DroneNetwork.OrdineResponse risposta = stub.ordine(richiesta);

            // Chiudo il canale
            canale.shutdown();
          }
        }
      }

      droneMain.listaOrdiniInUtilizzo = false;
      synchronized (droneMain.listaOrdiniLock) {
        droneMain.listaOrdiniLock.notifyAll();
      }
    }
  }
}
