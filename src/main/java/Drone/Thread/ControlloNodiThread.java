package Drone.Thread;

import Drone.DroneMain;
import beans.Drone_bean;
import io.grpc.*;
import java.util.concurrent.TimeUnit;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.PingDroneGrpc;

public class ControlloNodiThread implements Runnable {
  private final DroneMain droneMain;

  public ControlloNodiThread(DroneMain drone) {
    this.droneMain = drone;
  }

  private final boolean debugging = false;

  @Override
  public void run() {

    while (true) {

      if (droneMain.getDroneListBean().countdrones() > 1) {
        if (debugging) {
          System.out.println("\nHo un drone successivo.");
        }

        Boolean richiestaVotazione = false;

        Drone_bean droneSuccessivo;

        int numeroDroni = droneMain.getDroneListBean().countdrones();
        int offset = 0;

        while (numeroDroni > 1) {

          synchronized (droneMain.droneListBeanLock) {
            while (droneMain.droneListBeanInUtilizzo) {
              try {

                droneMain.droneListBeanLock.wait();

              } catch (InterruptedException err) {
                err.printStackTrace();
              }
            }
          }
          droneMain.droneListBeanInUtilizzo = true;

          droneSuccessivo =
              droneMain.getDroneListBean().getDroneSuccessivo(droneMain.getId(), offset);

          offset += 1;

          final ManagedChannel canale =
              ManagedChannelBuilder.forTarget(
                      "localhost:" + droneSuccessivo.getPortaComunicazioneDroni())
                  .usePlaintext()
                  .build();

          try {
            // Creo un blocco stub sul canale
            PingDroneGrpc.PingDroneBlockingStub stub = PingDroneGrpc.newBlockingStub(canale);

            DroneNetwork.PingDroneMessage richiesta;

            richiesta =
                DroneNetwork.PingDroneMessage.newBuilder()
                    .setIdDrone(droneMain.getId())
                    .setTimestampLogico(droneMain.getTimeStampLogico())
                    .build();

            if (debugging) {
              System.out.println("\nEffettuo ping al drone n° " + droneSuccessivo.getid());
            }

            DroneNetwork.PingDroneMessage risposta =
                stub.withDeadlineAfter(1000, TimeUnit.MILLISECONDS).pingDrone(richiesta);
            if (debugging) {
              System.out.println("Ricevo risposta del ping.");
            }
            droneMain.droneListBeanInUtilizzo = false;
            synchronized (droneMain.droneListBeanLock) {
              droneMain.droneListBeanLock.notifyAll();
            }
          } catch (StatusRuntimeException e) {

            System.out.println(
                "Deadline finita. Il drone n° " + droneSuccessivo.getid() + " non risponde più.");

            System.out.println(
                "\nLista droni prima dell'eliminazione: \n"
                    + droneMain.getDroneListBean().toString());
            if (droneSuccessivo.getid() == droneMain.getIdMasterDrone()) {
              System.out.println(
                  "Il drone in questione è il drone Master: lo elimino dalla struttura dati ed avvio una nuova elezione.\n");
              droneMain.eliminaDroneDaStrutturaDati(droneSuccessivo.getid());
              System.out.println(
                  "Lista droni dopo l'eliminazione: \n" + droneMain.getDroneListBean().toString());
              droneMain.setIdMasterDrone(-1);

              richiestaVotazione = true; // Quindi dopo avvio una nuova elezione

            } else {
              System.out.println(
                  "Il drone in questione NON è il drone Master: lo elimino dalla struttura dati.");
              droneMain.eliminaDroneDaStrutturaDati(droneSuccessivo.getid());
              System.out.println(
                  "\nLista droni dopo l'eliminazione: \n"
                      + droneMain.getDroneListBean().toString());
            }

            droneMain.droneListBeanInUtilizzo = false;
            synchronized (droneMain.droneListBeanLock) {
              droneMain.droneListBeanLock.notifyAll();
            }
          }

          canale.shutdown();

          try {
            Thread.sleep(500);
          } catch (InterruptedException e) {
            e.printStackTrace();
          }

          numeroDroni -= 1;
        }

        if (richiestaVotazione) {
          droneMain.avviaElezione();
        }

      } else {
        if (debugging) {
          System.out.println("\nNon ho nessun drone successivo: non effettuo il ping.");
        }

        try {
          Thread.sleep(500);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
