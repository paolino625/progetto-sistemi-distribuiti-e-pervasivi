package Drone.Thread;

import Drone.DroneMain;

public class StampaInfoDroneThread implements Runnable {

  private final DroneMain droneMain;

  public StampaInfoDroneThread(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void run() {
    while (true) {

      // Aspetto 10 secondi

      try {
        Thread.sleep(10000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      if (!droneMain.ricaricaBatteriaInCorso) {

        System.out.println(
            "\n-----------------------------------------------------------------------\n\nINFORMAZIONI DRONE\n\nNumero totale di consegne effettuate: "
                + droneMain.getNumeroTotaleConsegne()
                + "\nChilometri percorsi: "
                + droneMain.getChilometriEffettuatiTotali()
                + "\nLivello batteria: "
                + droneMain.getLivelloBatteria()
                + "%"
                + "\n\n-----------------------------------------------------------------------\n");
      }
    }
  }
}
