package Drone;

import Drone.ServerGRPC.ServerDroneThread;
import Drone.Thread.*;
import Simulatore.PM10Simulator;
import java.util.Random;
import java.util.Scanner;

public class StartDrone {

  static Thread assegnaOrdini, inviaStatisticheGlobali;

  public static void main(String[] args) throws InterruptedException {

    String indirizzoServerAmministratore = "http://localhost:1337";
    Random rand = new Random();
    int idDrone;
    int portaComunicazioneDroni = rand.nextInt(2000) + 8080;

    System.out.println("Avvio Drone in corso...\n");

    Scanner scanner = new Scanner(System.in);

    DroneMain drone = null;

    do {
      System.out.print("Inserisci l'ID del drone (0 per scelta casuale): ");

      try {
        int interoLetto = scanner.nextInt();

        if (interoLetto == 0) {
          idDrone = rand.nextInt(1000);
          System.out.println("ID creato casualmente.\n");

        } else {
          idDrone = interoLetto;
          System.out.println("ID impostato correttamente.\n");
        }

        drone = new DroneMain(idDrone, portaComunicazioneDroni, indirizzoServerAmministratore);

        System.out.println(drone.toString());
      } catch (Exception e) {
        // e.printStackTrace();
        scanner.nextLine();
      }

    } while (drone == null
        || !drone
            .registrazioneServerAmministratore()); // Registrazione al server Rest e ricevo lista
    // dei
    // droni e la mia posizione

    Thread generazioneDatiInquinamento =
        new Thread(new PM10Simulator(drone.getSensoreInquinamentoBuffer()));
    generazioneDatiInquinamento.start();

    Thread serverDroneThread = new Thread(new ServerDroneThread(drone, portaComunicazioneDroni));
    serverDroneThread.start(); // Avvio il server che rimane in ascolto di chiamate gRPC

    Thread.sleep(2000);
    drone.entraNellaRete(); // Il drone si inserisce autonomamente nella rete distribuita

    Thread letturaUscitaTerminale = new Thread(new LetturaTerminaleThread(drone, scanner));
    letturaUscitaTerminale.start();

    Thread letturaUscitaBatteria = new Thread(new LetturaUscitaBatteriaThread(drone));
    letturaUscitaBatteria.start();

    Thread controlloDroneSuccessivo = new Thread(new ControlloNodiThread(drone));
    controlloDroneSuccessivo.start();

    Thread letturaServerDronaZone = new Thread(new LetturaServerDronaZoneThread(drone));
    letturaServerDronaZone.start();

    Thread stampaInfoDrone = new Thread(new StampaInfoDroneThread(drone));
    stampaInfoDrone.start();

    assegnaOrdini = new Thread(new AssegnaOrdiniThread(drone));
    assegnaOrdini.start();

    inviaStatisticheGlobali = new Thread(new InviaStatisticheGlobaliThread(drone));
    inviaStatisticheGlobali.start();
  }

  public static Thread getAssegnaOrdiniThread() {
    return assegnaOrdini;
  }

  public static Thread getInviaStatisticheGlobaliThread() {
    return inviaStatisticheGlobali;
  }
}
