package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.Context;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.PingDroneGrpc;

public class PingDroneImpl extends PingDroneGrpc.PingDroneImplBase {

  private final int idDrone;
  private final DroneMain droneMain;

  private final boolean debugging = false;

  public PingDroneImpl(DroneMain droneMain, int idDrone) {
    this.droneMain = droneMain;
    this.idDrone = idDrone;
  }

  @Override
  public void pingDrone(
      DroneNetwork.PingDroneMessage request,
      StreamObserver<DroneNetwork.PingDroneMessage> responseObserver) {
    if (Context.current().isCancelled()) {
      responseObserver.onError(
          Status.CANCELLED.withDescription("Cancellata dal cliente").asRuntimeException());
      return;
    }

    if (debugging) {

      System.out.println(
          "\nMessaggio in arrivo dal server dal drone n° "
              + request.getIdDrone()
              + "\nTipologia Messaggio: PingDrone.");
    }

    DroneNetwork.PingDroneMessage risposta;

    risposta =
        DroneNetwork.PingDroneMessage.newBuilder()
            .setIdDrone(idDrone)
            .setTimestampLogico(droneMain.getTimeStampLogico())
            .build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    if (debugging) {
      System.out.print("Risposta inviata!\n");
    }
  }
}
