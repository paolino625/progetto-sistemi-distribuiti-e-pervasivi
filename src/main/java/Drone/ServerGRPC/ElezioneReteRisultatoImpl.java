package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.ElezioneReteRisultatoGrpc;

public class ElezioneReteRisultatoImpl
    extends ElezioneReteRisultatoGrpc.ElezioneReteRisultatoImplBase {
  private final DroneMain droneMain;

  public ElezioneReteRisultatoImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void elezioneReteRisultato(
      DroneNetwork.ElezioneReteRisultatoRequest request,
      StreamObserver<DroneNetwork.ElezioneReteRisultatoResponse> responseObserver) {
    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDroneMaster()
            + "\nTipologia Messaggio: RisultatoElezione.");

    DroneNetwork.ElezioneReteRisultatoResponse risposta;

    risposta =
        DroneNetwork.ElezioneReteRisultatoResponse.newBuilder()
            .setIdDrone(droneMain.getId())
            .build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n\n");

    droneMain.continuaPubblicazioneElezioneRisultato(request.getIdDroneMaster());
  }
}
