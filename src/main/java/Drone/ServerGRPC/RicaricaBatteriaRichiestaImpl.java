package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.RicaricaBatteriaRichiestaGrpc;

public class RicaricaBatteriaRichiestaImpl
    extends RicaricaBatteriaRichiestaGrpc.RicaricaBatteriaRichiestaImplBase {

  private final DroneMain droneMain;

  public RicaricaBatteriaRichiestaImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void ricaricaBatteriaRichiesta(
      DroneNetwork.RicaricaBatteriaRichiestaRequest request,
      StreamObserver<DroneNetwork.RicaricaBatteriaRichiestaResponse> responseObserver) {

    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: RicaricaBatteriaRichiesta.");

    /*

    Algoritmo Ricart and Agrawala

    Quando un processo Q riceve un messaggio abbiamo 3 casi:
    - Se Q non sta usando R e non richiede R, risponde OK a P.
    - Se Q sta usando R non risponde e mette in coda la richiesta.
    - Se Q vuole usare R ma non lo ha ancora fatto, compara il timestamp del messaggio con quello nel messaggio che ha mandato con la sua richiesta.
    - Il timestamp più piccolo vince. Se quello nel messaggio mandato da P è più piccolo risponde OK a P, altrimenti mette in coda il messaggio.

     */

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    System.out.println("\nApplico algoritmo Ricart and Agrawala...");

    if (!droneMain.ricaricaBatteriaInCorso && !droneMain.richiestaRicaricaBatteria) {
      System.out.println("Non sto ricaricando la batteria e non voglio ricaricarla: invio OK.");
      droneMain.inviaConfermaRichiestaBatteria(request.getIdDrone());

    } else if (droneMain.ricaricaBatteriaInCorso) {
      System.out.println("Sto ricaricando la batteria: aggiungo il drone alla coda dei droni.");
      droneMain.getDroniACuiDevoInviareConferma().add(request.getIdDrone());

    } else {
      System.out.println(
          "Non sto ricaricando la batteria ma voglio ricaricarla: confronto i timestamp.");
      if (timestampMinoreOrdineTotale(droneMain, request) == 1) {
        System.out.println(
            "Ho timestamp globale minore della richiesta, dunque aggiungo il drone alla coda dei droni.");
        droneMain.getDroniACuiDevoInviareConferma().add(request.getIdDrone());

      } else {
        System.out.println("Ho timestamp globale maggiore della richiesta: invio OK.");
        droneMain.inviaConfermaRichiestaBatteria(request.getIdDrone());
      }
    }

    DroneNetwork.RicaricaBatteriaRichiestaResponse risposta;

    risposta = DroneNetwork.RicaricaBatteriaRichiestaResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n");
  }

  private int timestampMinoreOrdineTotale(
      DroneMain droneMain, DroneNetwork.RicaricaBatteriaRichiestaRequest request) {
    if (droneMain.getTimeStampLogico() < request.getTimestampLogico()) {
      return 1;
    } else if (droneMain.getTimeStampLogico() > request.getTimestampLogico()) {
      return 2;
    } else {
      if (droneMain.getId() < request.getIdDrone()) {
        return 1;
      } else {
        return 2;
      }
    }
  }
}
