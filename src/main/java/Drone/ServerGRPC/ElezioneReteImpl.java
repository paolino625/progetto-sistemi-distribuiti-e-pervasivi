package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.ElezioneReteGrpc;

public class ElezioneReteImpl extends ElezioneReteGrpc.ElezioneReteImplBase {
  private final DroneMain droneMain;

  public ElezioneReteImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void elezioneRete(
      DroneNetwork.ElezioneRequest request,
      StreamObserver<DroneNetwork.ElezioneResponse> responseObserver) {
    System.out.println(
        "\nMessaggio in arrivo dal server dal drone precedente.\nTipologia Messaggio: ElezioneRete.");

    DroneNetwork.ElezioneResponse risposta;

    risposta = DroneNetwork.ElezioneResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n\n");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    droneMain.continuaElezione(request.getIdDrone(), request.getLivelloBatteria());
  }
}
