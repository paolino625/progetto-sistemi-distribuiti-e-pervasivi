package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.AggiornamentoInfoDroneGrpc;
import paolo.calcagni.DroneNetwork.DroneNetwork;

public class AggiornamentoInfoDroneImpl
    extends AggiornamentoInfoDroneGrpc.AggiornamentoInfoDroneImplBase {
  private final DroneMain droneMain;

  public AggiornamentoInfoDroneImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void aggiornamentoInfoDrone(
      DroneNetwork.AggiornamentoInfoDroneRequest request,
      StreamObserver<DroneNetwork.AggiornamentoInfoDroneResponse> responseObserver) {

    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: AggiornamentoInfoDrone.");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    droneMain.aggiornaInfoDrone(
        request.getIdDrone(),
        request.getLivelloBatteria(),
        request.getCoordinataX(),
        request.getCoordinataY());

    DroneNetwork.AggiornamentoInfoDroneResponse risposta;

    risposta = DroneNetwork.AggiornamentoInfoDroneResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n\n");
  }
}
