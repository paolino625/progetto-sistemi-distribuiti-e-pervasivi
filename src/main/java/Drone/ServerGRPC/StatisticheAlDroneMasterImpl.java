package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.StatisticheAlDroneMasterGrpc;

public class StatisticheAlDroneMasterImpl
    extends StatisticheAlDroneMasterGrpc.StatisticheAlDroneMasterImplBase {

  private final DroneMain droneMain;

  public StatisticheAlDroneMasterImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void statisticheAlDroneMaster(
      DroneNetwork.StatisticheAlDroneMasterRequest request,
      StreamObserver<DroneNetwork.StatisticheAlDroneMasterResponse> responseObserver) {
    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: StatisticheAlDroneMaster.");

    synchronized (droneMain.droneListBeanLock) {
      while (droneMain.droneListBeanInUtilizzo) {
        try {

          droneMain.droneListBeanLock.wait();

        } catch (InterruptedException err) {
          err.printStackTrace();
        }
      }
    }
    droneMain.droneListBeanInUtilizzo = false;

    DroneNetwork.StatisticheAlDroneMasterResponse risposta;

    risposta = DroneNetwork.StatisticheAlDroneMasterResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    droneMain.getDroneListBean().getbyid(request.getIdDrone()).setIdOrdineInGestione(-1);

    synchronized (droneMain.listaOrdiniLock) {
      while (droneMain.listaOrdiniInUtilizzo) {
        try {

          droneMain.listaOrdiniLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    droneMain.listaOrdiniInUtilizzo = true;

    droneMain.eliminaOrdine(request.getIdOrdine());

    droneMain.listaOrdiniInUtilizzo = false;
    synchronized (droneMain.listaOrdiniLock) {
      droneMain.listaOrdiniLock.notifyAll();
    }

    droneMain.aggiornaStatistiche(
        request.getIdDrone(),
        request.getLivelloBatteria(),
        request.getPosizioneCoordinataX(),
        request.getPosizioneCoordinataY(),
        request.getTimestamp(),
        request.getChilometriEffettuati(),
        request.getMediaMisurazioniInquinamento());

    droneMain.droneListBeanInUtilizzo = false;
    synchronized (droneMain.droneListBeanLock) {
      droneMain.droneListBeanLock.notifyAll();
    }
  }
}
