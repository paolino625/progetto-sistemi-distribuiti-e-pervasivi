package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.Server;
import io.grpc.ServerBuilder;

public class ServerDroneThread implements Runnable {

  private final DroneMain droneMain;
  private int porta;

  public ServerDroneThread(DroneMain droneMain, int porta) {
    this.droneMain = droneMain;
    this.porta = porta;
  }

  @Override
  public void run() {
    try {

      Server server =
          ServerBuilder.forPort(porta)
              .addService(new PresentazioneDroneImpl(droneMain))
              .addService(new PingDroneImpl(droneMain, droneMain.getId()))
              .addService(new ElezioneReteImpl(droneMain))
              .addService(new ElezioneReteRisultatoImpl(droneMain))
              .addService(new AggiornamentoInfoDroneImpl(droneMain))
              .addService(new StatisticheAlDroneMasterImpl(droneMain))
              .addService(new OrdineImpl(droneMain))
              .addService(new RicaricaBatteriaRichiestaImpl(droneMain))
              .addService(new RicaricaBatteriaRispostaImpl(droneMain))
              .build();
      server.start();
      System.out.println("\nServer gRPC partito sulla porta " + porta);

      server.awaitTermination();

    } catch (Exception e) {

      System.out.println("ERRORE SERVER!");
      e.printStackTrace();
    }

    System.out.println("SERVER TERMINATO!");
  }
}
