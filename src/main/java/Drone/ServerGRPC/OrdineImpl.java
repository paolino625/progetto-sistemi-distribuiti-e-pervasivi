package Drone.ServerGRPC;

import ClassiUtili.Ordine;
import Drone.DroneMain;
import beans.PuntoMappa_bean;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.OrdineGrpc;

public class OrdineImpl extends OrdineGrpc.OrdineImplBase {
  private final DroneMain droneMain;

  public OrdineImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void ordine(
      DroneNetwork.OrdineRequest request,
      StreamObserver<DroneNetwork.OrdineResponse> responseObserver) {
    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: Ordine.");

    DroneNetwork.OrdineResponse risposta;

    risposta = DroneNetwork.OrdineResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n\n");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    PuntoMappa_bean puntoRitiro =
        new PuntoMappa_bean(
            request.getPuntoRitiroCoordinataX(), request.getPuntoRitiroCoordinataY());
    PuntoMappa_bean puntoConsegna =
        new PuntoMappa_bean(
            request.getPuntoConsegnaCoordinataX(), request.getPuntoConsegnaCoordinataY());
    Ordine ordine = new Ordine(request.getIdOrdine(), puntoRitiro, puntoConsegna);

    synchronized (droneMain.consegnaInCorsoLock) {
      while (droneMain.consegnaInCorso) {
        try {

          droneMain.consegnaInCorsoLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    droneMain.consegnaInCorso = true;

    synchronized (droneMain.listaOrdiniLock) {
      while (droneMain.listaOrdiniInUtilizzo) {
        try {

          droneMain.listaOrdiniLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    droneMain.listaOrdiniInUtilizzo = true;

    droneMain.consegnaOrdine(ordine);

    droneMain.inviaStatisticheAlDroneMaster(ordine.getOrdineID());

    droneMain.listaOrdiniInUtilizzo = false;
    synchronized (droneMain.listaOrdiniLock) {
      droneMain.listaOrdiniLock.notifyAll();
    }

    droneMain.consegnaInCorso = false;
    synchronized (droneMain.consegnaInCorsoLock) {
      droneMain.consegnaInCorsoLock.notifyAll();
    }
  }
}
