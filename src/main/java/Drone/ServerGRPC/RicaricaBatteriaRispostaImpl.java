package Drone.ServerGRPC;

import Drone.DroneMain;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.RicaricaBatteriaRispostaGrpc;

public class RicaricaBatteriaRispostaImpl
    extends RicaricaBatteriaRispostaGrpc.RicaricaBatteriaRispostaImplBase {

  private final DroneMain droneMain;

  public RicaricaBatteriaRispostaImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void ricaricaBatteriaRisposta(
      DroneNetwork.RicaricaBatteriaRispostaRequest request,
      StreamObserver<DroneNetwork.RicaricaBatteriaRispostaResponse> responseObserver) {

    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: RicaricaBatteriaRisposta.");

    DroneNetwork.RicaricaBatteriaRispostaResponse risposta;

    risposta = DroneNetwork.RicaricaBatteriaRispostaResponse.newBuilder().build();

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n");

    System.out.println(
        "\nRicevuto OK dal drone n° "
            + request.getIdDrone()
            + ": lo rimuovo dalla lista dei droni di cui attendo conferma.");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());

    droneMain
        .getDroniDiCuiAttendoConferma()
        .remove(droneMain.getDroniDiCuiAttendoConferma().indexOf(request.getIdDrone()));

    if (droneMain.getDroniDiCuiAttendoConferma().size() == 0) {

      System.out.println(
          "Non ci sono più droni di cui attendo l'OK: dunque posso ricaricare la batteria.");
      droneMain.ricaricaBatteria();
    }
  }
}
