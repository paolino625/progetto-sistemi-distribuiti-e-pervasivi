package Drone.ServerGRPC;

import Drone.DroneMain;
import beans.DroneList_bean;
import beans.Drone_bean;
import io.grpc.stub.StreamObserver;
import paolo.calcagni.DroneNetwork.DroneNetwork;
import paolo.calcagni.DroneNetwork.PresentazioneDroneGrpc;

public class PresentazioneDroneImpl extends PresentazioneDroneGrpc.PresentazioneDroneImplBase {
  private final DroneMain droneMain;

  public PresentazioneDroneImpl(DroneMain drone) {
    this.droneMain = drone;
  }

  @Override
  public void presentazioneDrone(
      DroneNetwork.PresentazioneDroneRequest request,
      StreamObserver<DroneNetwork.PresentazioneDroneResponse> responseObserver) {

    synchronized (droneMain.elezioneLock) {
      while (droneMain.elezioneInCorso) {
        try {

          droneMain.elezioneLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    droneMain.elezioneInCorso = true;

    // La richiesta è di tipo PresentazioneDroneRequest (definito in .proto)
    System.out.println(
        "\nMessaggio in arrivo dal server dal drone n° "
            + request.getIdDrone()
            + "\nTipologia Messaggio: Presentazione Drone.\n");

    // Memorizzo qui la lista dei droni che ho.
    DroneList_bean droneList = droneMain.getDroneListBean();
    System.out.println(
        "Lista droni prima della registrazione del nuovo drone:\n" + droneList.toString());

    // Memorizzo in una nuova variabile le informazioni del nuovo Drone.
    Drone_bean drone =
        new Drone_bean(
            request.getIdDrone(),
            request.getPortaDrone(),
            request.getIndirizzoDrone(),
            request.getCoordinataX(),
            request.getCoordinataY());

    // Aggiungo il drone alla lista dei droni che ho.
    droneList.add(drone);

    System.out.println(
        "Lista droni dopo la registrazione del nuovo drone: \n" + droneList.toString());

    DroneNetwork.PresentazioneDroneResponse risposta;

    if (request.getNodoSuccessivo()) {
      System.out.println("Dato che sono il drone successivo invio l'ID del master.");
      risposta =
          DroneNetwork.PresentazioneDroneResponse.newBuilder()
              .setIdDroneMaster(droneMain.getIdMasterDrone())
              .build();

    } else {

      System.out.println("Dato che non sono il drone successivo invio una risposta vuota.");
      risposta = DroneNetwork.PresentazioneDroneResponse.newBuilder().setIdDroneMaster(-1).build();
    }

    synchronized (droneMain.elezioneLock) {
      droneMain.elezioneLock.notifyAll();
    }
    droneMain.elezioneInCorso = false;

    responseObserver.onNext(risposta);
    responseObserver.onCompleted();
    System.out.print("Risposta inviata!\n");

    droneMain.aggiornaTimestampLogicoRicezioneMessaggio(request.getTimestampLogico());
  }

  private Boolean sonoSuccessivoDelDrone(Drone_bean drone) {
    DroneList_bean listaDroni = droneMain.getDroneListBean();
    listaDroni.ordina();
    System.out.println("Dopo ordinamento: \n" + listaDroni.toString());

    if (listaDroni.getArrayListDrones().size() <= 1) {
      System.out.println("C'è un altro solo drone nella rete: dunque sono il successivo.");
      return true;
    } else {
      int indiceDrone = listaDroni.getArrayListDrones().indexOf(drone);
      System.out.print("Indice del drone nell'arrayList: " + indiceDrone + "\n");

      if (listaDroni
              .getArrayListDrones()
              .get((indiceDrone + 1) % listaDroni.getArrayListDrones().size())
              .getid()
          == droneMain.getId()) {
        System.out.println("\nSono il successivo!");
        return true;
      } else {
        System.out.println("\nNon sono il successivo!");
        return false;
      }
    }
  }
}
