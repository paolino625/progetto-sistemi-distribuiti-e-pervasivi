/*

Droni slave e master stessa classe con una sorta di flag

Ogni drone è sia client che server.

Ogni drone deve avere una rappresentazione degli altri droni: la cosa più difficile è mantenere questa rappresentazione consistente.
Magari per gestire questa consistenza può bastare che ogni drone conosca solo il proprio vicino.

# MODULI

- Thread per gestire standard input
- Modulo per gestire topologia di rete e elezione del master
- Modulo per generare le statistiche delle consegne
- Modulo per gestire il sensore PM10

 */

package Drone;

import ClassiUtili.CalcoloDistanzaDuePunti;
import ClassiUtili.Ordine;
import Drone.Thread.InviaMessaggioPresentazioneThread;
import Simulatore.Measurement;
import beans.DroneList_bean;
import beans.Drone_bean;
import beans.PuntoMappa_bean;
import beans.StatisticheGlobali_bean;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import paolo.calcagni.DroneNetwork.*;

public class DroneMain {

  private int id = -1;
  private final String indirizzoDrone = "localhost";
  private int portaComunicazioneDroni = -1;
  private String indirizzoServerAmministratore = "-1";
  private PuntoMappa_bean posizioneAttuale = new PuntoMappa_bean(-1, -1);

  private DroneList_bean droneListBean;
  public volatile Boolean droneListBeanInUtilizzo = false;
  public final Object droneListBeanLock = new Object();

  public boolean elezioneInCorso = false;
  public final Object elezioneLock = new Object();

  public boolean partecipanteElezione = false;

  private int idMasterDrone = -1;

  private final ArrayList<Ordine> listaOrdini = new ArrayList<>();
  public volatile Boolean listaOrdiniInUtilizzo = false;
  public final Object listaOrdiniLock = new Object();

  private int chilometriEffettuatiUltimaStatistica;
  private int chilometriEffettuatiTotali = 0;

  private StatisticheGlobali_bean statisticheGlobali = new StatisticheGlobali_bean();

  private final SensoreInquinamentoBuffer sensoreInquinamentoBuffer =
      new SensoreInquinamentoBuffer(this);
  private double mediaMisurazioniInquinamento;

  public volatile Boolean richiestaChiusuraDrone = false;

  public volatile Boolean invioUltimeStatisticheAlServer = false;

  private int numeroTotaleConsegne = 0;

  public volatile Boolean consegnaInCorso = false;
  public final Object consegnaInCorsoLock = new Object();

  public ArrayList<Integer> droniDiCuiAttendoConferma = new ArrayList<>();
  public ArrayList<Integer> droniACuiDevoInviareConferma = new ArrayList<>();

  public volatile int timeStampLogico = 0;

  public volatile Boolean ricaricaBatteriaInCorso = false;
  public volatile Boolean richiestaRicaricaBatteria = false;
  public final Object ricaricaBatteriaLock = new Object();

  private final boolean sleepAttive = false;

  public DroneMain(int id, int portaComunicazioneDroni, String indirizzoServerAmministratore) {
    this.id = id;
    this.portaComunicazioneDroni = portaComunicazioneDroni;
    this.indirizzoServerAmministratore = indirizzoServerAmministratore;
  }

  public Boolean registrazioneServerAmministratore() {

    Client client = Client.create();
    ClientResponse clientResponse = null;

    System.out.println("\nInvio richiesta POST al Server REST.");
    int htmlResponse =
        aggiungoDroneAlServer(
            client,
            clientResponse); // Mando richiesta POST al Server REST richiedendo di aggiungere il
    // drone

    if (htmlResponse == 200) {
      System.out.println("Drone registrato con successo presso il server REST.");
      ricevoInformazioniDalServer(
          client, clientResponse); // Ricevo lista droni e posizione del mio drone
      return true;
    } else if (htmlResponse == 409) {
      System.err.println("Esiste già un drone con lo stesso ID.");
      return false;
    } else {
      System.err.println("Errore n° " + htmlResponse);
      return false;
    }
  }

  private int aggiungoDroneAlServer(Client client, ClientResponse clientResponse) {
    // Mando informazioni del drone attuale al Server tramite richiesta POST.
    String postPath = "/gestioneDroni/aggiungiDrone";
    Drone_bean drone = new Drone_bean(id, portaComunicazioneDroni, "localhost", -1, -1);
    clientResponse = postRequest(client, indirizzoServerAmministratore + postPath, drone);
    if (clientResponse != null) {
      System.out.println("Risposta del Server REST: " + clientResponse.toString());
      return clientResponse.getStatus();
    } else {
      System.err.println("Il server non è attivo.");
      return 404;
    }
  }

  private void ricevoInformazioniDalServer(Client client, ClientResponse clientResponse) {

    getDronesList(client);
    System.out.println(
        "Ho ricevuto dal Server la seguente lista dei droni:\n" + droneListBean.toString());

    System.out.println("Chiedo la mia posizione di partenza al Server");
    posizioneAttuale = getPosizioneDrone(client);

    System.out.println("La mia posizione è:\n" + posizioneAttuale.toString());
  }

  private void getDronesList(Client client) { // Inizializzo la lista degli altri droni.

    String getPath = "/gestioneDroni";
    ClientResponse clientResponse = getRequest(client, indirizzoServerAmministratore + getPath);
    System.out.println("\n" + clientResponse.toString());
    droneListBean = clientResponse.getEntity(DroneList_bean.class);
    // System.out.println(arrayListDroni.toString());
  }

  public synchronized DroneList_bean getDroneListBean() {
    return droneListBean;
  }

  private PuntoMappa_bean getPosizioneDrone(Client client) {

    String getPath = "/gestioneDroni/getDrone/" + id;
    ClientResponse clientResponse = getRequest(client, indirizzoServerAmministratore + getPath);
    System.out.println(clientResponse.toString());
    Drone_bean drone = clientResponse.getEntity(Drone_bean.class);
    PuntoMappa_bean posizionePartenza =
        new PuntoMappa_bean(
            drone.getPosizioneDroneCoordinataX(), drone.getPosizioneDroneCoordinataY());

    return posizionePartenza;
  }

  // Metodo di utilità necessario per metodi post
  public static ClientResponse postRequest(Client client, String url, Drone_bean drone) {
    WebResource webResource = client.resource(url);
    String input = new Gson().toJson(drone);
    System.out.println("\nJSON inviato: " + input);
    try {
      return webResource.type("application/json").post(ClientResponse.class, input);
    } catch (ClientHandlerException e) {
      System.err.println("Server non disponibile.");
      return null;
    }
  }

  // Metodo di utilità necessario per metodi post
  public static ClientResponse postRequestStatistiche(
      Client client, String url, StatisticheGlobali_bean statisticheGlobali) {
    WebResource webResource = client.resource(url);
    String input = new Gson().toJson(statisticheGlobali);
    System.out.println("\nJSON inviato: " + input);
    try {
      return webResource.type("application/json").post(ClientResponse.class, input);
    } catch (ClientHandlerException e) {
      System.err.println("Server non disponibile");
      return null;
    }
  }

  // Metodo di utilità necessario per metodi get
  public static ClientResponse getRequest(Client client, String url) {
    WebResource webResource = client.resource(url);
    try {
      return webResource.type("application/json").get(ClientResponse.class);
    } catch (ClientHandlerException e) {
      System.err.println("Server non disponibile");
      return null;
    }
  }

  // Metodo di utilità necessario per metodi delete
  public static ClientResponse deleteRequest(Client client, String url) {
    WebResource webResource = client.resource(url);
    try {
      return webResource.type("application/json").delete(ClientResponse.class);
    } catch (ClientHandlerException e) {
      System.err.println("Server non disponibile");
      return null;
    }
  }

  public void entraNellaRete() {

    System.out.println("\nProvo ad entrare nella rete");

    // Se sono l'unico drone nella rete allora sono anche il master
    if (droneListBean.countdrones() == 1) {
      System.out.println("Sono l'unico drone nella rete: mi autoproclamo master.");
      setIdMasterDrone(this.id);
    } else {
      System.out.println("Non sono l'unico drone nella rete: mi presento agli altri droni.\n");
      presentazioneAllaRete(); // Se non sono l'unico drone allora mi presento a tutti gli altri.
    }
  }

  private void presentazioneAllaRete() {

    ArrayList<Thread> listaThread = new ArrayList<>();

    // Creo un thread per l'invio di ogni messaggio
    System.out.println("Mi presento alla rete: invio i messaggi in broadcast in parallelo.");

    int indiceNodoSuccessivo =
        (droneListBean.getArrayListDrones().indexOf(droneListBean.getbyid(id)) + 1)
            % droneListBean.getArrayListDrones().size();
    int idNodoSuccessivo = droneListBean.getArrayListDrones().get(indiceNodoSuccessivo).getid();

    for (Drone_bean drone : droneListBean.getArrayListDrones()) {

      if (drone.getid() != this.id) {

        aggiornaTimestampLogicoInvioMessaggio();

        if (drone.getid() == idNodoSuccessivo) {
          listaThread.add(new Thread(new InviaMessaggioPresentazioneThread(this, drone, true)));
        } else {
          listaThread.add(new Thread(new InviaMessaggioPresentazioneThread(this, drone, false)));
        }
      }
    }

    // Avvio tutti i thread
    System.out.println("Avvio tutti i thread.");
    for (Thread t : listaThread) {
      t.start();
    }

    // Aspetto che tutti i messaggi vengano consegnati
    System.out.println("Rimango in attesa che i thread terminino\n");
    for (Thread t : listaThread) {
      try {
        t.join();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println("Tutti i thread terminati.");
  }

  public String toString() {

    String stringa =
        "ID Drone: "
            + id
            + "\nIndirizzo Drone: "
            + indirizzoDrone
            + "\nPorta Comunicazione Droni: "
            + portaComunicazioneDroni
            + "\nIndirizzo Server Amministratore: "
            + indirizzoServerAmministratore
            + "\nPosizione di Partenza: \n"
            + posizioneAttuale.toString()
            + "\n";

    if (droneListBean != null) {
      stringa += "Array List Droni:" + droneListBean.toString();
    }

    return stringa;
  }

  public int getIdMasterDrone() {
    return idMasterDrone;
  }

  public int getId() {
    return id;
  }

  public void avviaElezione() {

    if (!elezioneInCorso) {
      synchronized (elezioneLock) {
        while (elezioneInCorso) {
          try {

            elezioneLock.wait();

          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }

      elezioneInCorso = true;
      partecipanteElezione = true;
    }

    Drone_bean droneSuccessivo = droneListBean.getDroneSuccessivo(id, 0);

    System.out.println(
        "Avvio una nuova elezione mandando il messaggio di elezione al drone successivo n° "
            + droneSuccessivo.getid());

    aggiornaTimestampLogicoInvioMessaggio();

    // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + droneSuccessivo.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    // Creo un blocco stub sul canale
    ElezioneReteGrpc.ElezioneReteBlockingStub stub = ElezioneReteGrpc.newBlockingStub(canale);

    DroneNetwork.ElezioneRequest richiesta =
        DroneNetwork.ElezioneRequest.newBuilder()
            .setIdDrone(id)
            .setLivelloBatteria(getLivelloBatteriaReale())
            .setTimestampLogico(getTimeStampLogico())
            .build();

    System.out.println(
        "Effettuo richiesta gRPC alla porta: " + droneSuccessivo.getPortaComunicazioneDroni());
    DroneNetwork.ElezioneResponse risposta = stub.elezioneRete(richiesta);

    // Chiudo il canale
    canale.shutdown();
  }

  public int getLivelloBatteriaReale() {
    return getDroneListBean().getbyid(id).getLivelloBatteria();
  }

  public int getLivelloBatteria() {
    int livelloBatteria = getDroneListBean().getbyid(id).getLivelloBatteria();

    if (consegnaInCorso) {
      return livelloBatteria - 10;
    } else {
      return livelloBatteria;
    }
  }

  public void continuaElezione(int idDronePrecedente, int livelloBatteriaAltroDrone) {

    System.out.println(
        "Ho ricevuto un messaggio di elezione.\nID presente nel messaggio: "
            + idDronePrecedente
            + "\nBatteria presente nel messaggio: "
            + livelloBatteriaAltroDrone);

    if (id == idDronePrecedente) {
      System.out.println(
          "\nL'ID del messaggio è il mio ID, sono il nuovo Master!\nBlocco l'elezione e mando il messaggio con il risultato dell'elezione.");
      avviaPubblicazioneRisultatoElezione();
      return;
    }

    // Se è la prima volta che entro nell'elezione prendo il Lock, altrimenti ce l'ho già

    if (!elezioneInCorso) {
      synchronized (elezioneLock) {
        while (elezioneInCorso) {
          try {

            elezioneLock.wait();

          } catch (InterruptedException e) {
            e.printStackTrace();
          }
        }
      }
      elezioneInCorso = true;
    }

    if (sleepAttive) {
      try {
        System.out.println("\nCONTINUA ELEZIONE...\nATTENDO 6 SECONDI\n");
        Thread.sleep(8000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println("\nContinuo l'elezione...");

    Drone_bean droneSuccessivo = droneListBean.getDroneSuccessivo(id, 0);

    aggiornaTimestampLogicoInvioMessaggio();

    // Specifico indirizzo e porta del canale che offre il servizio
    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + droneSuccessivo.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    // Creo un blocco stub sul canale
    ElezioneReteGrpc.ElezioneReteBlockingStub stub = ElezioneReteGrpc.newBlockingStub(canale);

    DroneNetwork.ElezioneRequest richiesta;

    Boolean fermaElezione = false;

    if (getLivelloBatteria() > livelloBatteriaAltroDrone) {
      System.out.println(
          "Livello batteria di questo drone è maggiore: sostituisco ID del messaggio e livello batteria.");

      if (partecipanteElezione) {
        System.out.println("C'è già un'altra elezione in corso, non inoltro il messaggio.");
        fermaElezione = true;
      }

      richiesta =
          DroneNetwork.ElezioneRequest.newBuilder()
              .setIdDrone(id)
              .setLivelloBatteria(getLivelloBatteria())
              .setTimestampLogico(getTimeStampLogico())
              .build();

    } else if (getLivelloBatteria() == livelloBatteriaAltroDrone && id > idDronePrecedente) {
      System.out.println(
          "Livello batteria uguale ma ID di questo drone è maggiore: sostituisco ID del messaggio e livello batteria.");
      richiesta =
          DroneNetwork.ElezioneRequest.newBuilder()
              .setIdDrone(id)
              .setLivelloBatteria(getLivelloBatteria())
              .setTimestampLogico(getTimeStampLogico())
              .build();
    } else if (getLivelloBatteria() == livelloBatteriaAltroDrone && id < idDronePrecedente) {
      System.out.println(
          "Livello batteria uguale ma ID di questo drone è minore: lascio inalterato il messaggio.");
      richiesta =
          DroneNetwork.ElezioneRequest.newBuilder()
              .setIdDrone(idDronePrecedente)
              .setLivelloBatteria(livelloBatteriaAltroDrone)
              .setTimestampLogico(getTimeStampLogico())
              .build();
    } else {
      System.out.println(
          "Livello batteria di questo drone è minore: lascio inalterato il messaggio.");
      richiesta =
          DroneNetwork.ElezioneRequest.newBuilder()
              .setIdDrone(idDronePrecedente)
              .setLivelloBatteria(livelloBatteriaAltroDrone)
              .setTimestampLogico(getTimeStampLogico())
              .build();
    }

    partecipanteElezione = true;

    if (!fermaElezione) {

      System.out.println(
          "Effettuo richiesta gRPC alla porta: " + droneSuccessivo.getPortaComunicazioneDroni());
      DroneNetwork.ElezioneResponse risposta = stub.elezioneRete(richiesta);
    }

    // Chiudo il canale
    canale.shutdown();
  }

  public void esciDallaRete() {

    System.out.println("\nSto per uscire dalle rete...");

    // Disconnessione dal Broker MQTT
    System.out.println("Fermo Thread Subscriber MQTT.");
    richiestaChiusuraDrone = true;

    System.out.println("Verifico che non ci siano consegne in corso.");
    aspettaConsegnaInCorso();
    System.out.println("Ultima consegna completata.");

    // Se sono il drone master devo effettuare operazioni in più.
    if (sonoMaster()) {

      // Imposto a 0 la variabile ordineInGestione: in questo modo il Thread Assegna Ordini capisce
      // che tale drone è nella fase di spegnimento e non può assegnarli un ordine.
      getDroneListBean().getbyid(id).setIdOrdineInGestione(0);

      // Assicurarsi di assegnare le consegne pendenti ai droni della smart-city.
      try {
        System.out.println("Rimango in attesa di riuscire ad assegnare tutti gli ordini.");
        StartDrone.getAssegnaOrdiniThread()
            .join(); // Aspetto che il Thread Assegna Ordini finisca di consegnare tutti gli ordini.
        System.out.println("Tutti gli ordini sono stati assegnati correttamente.");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      // Informo il thread InviaStatisticheGlobaliThread che dovrà effettuare l'ultimo invio.
      invioUltimeStatisticheAlServer = true;
      try {
        System.out.println("Rimango in attesa dell'invio delle ultime statistiche al server.");
        StartDrone.getInviaStatisticheGlobaliThread()
            .join(); // Aspetto l'ultimo invio delle statistiche al server.
        System.out.println("Ultime statistiche inviate correttamente.");

      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    System.out.println("Elimino drone dal server.");
    eliminaDroneDalServer();
    System.out.println("Sono uscito dalla rete!");
    System.exit(0);
  }

  private void aspettaConsegnaInCorso() {
    synchronized (consegnaInCorsoLock) {
      while (consegnaInCorso) {
        try {

          consegnaInCorsoLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    consegnaInCorso = false;
  }

  public int eliminaDroneDalServer() {
    Client client = Client.create();
    ClientResponse clientResponse = null;

    // Mando informazioni del drone attuale al Server tramite richiesta POST.
    String postPath = "/gestioneDroni/rimuoviDrone/" + id;
    clientResponse = deleteRequest(client, indirizzoServerAmministratore + postPath);
    System.out.println("\nRisposta del Server REST: " + clientResponse.toString());
    return clientResponse.getStatus();
  }

  public void eliminaDroneDaStrutturaDati(int idDroneDaEliminare) {

    droneListBean.removebyid(idDroneDaEliminare);
  }

  public void setIdMasterDrone(int idMaster) {
    this.idMasterDrone = idMaster;
  }

  public void avviaPubblicazioneRisultatoElezione() {
    if (sleepAttive) {
      try {
        System.out.println("\nAVVIO PUBBLICAZIONE RISULTATO...\nATTENDO 8 SECONDI\n");
        Thread.sleep(8000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    /*
    this.elezioneInCorso = false;
    synchronized (elezioneLock) {
      elezioneLock.notifyAll();
    }*/

    partecipanteElezione = false;

    setIdMasterDrone(this.id);

    Drone_bean droneSuccessivo = droneListBean.getDroneSuccessivo(id, 0);

    System.out.println(
        "Invio risultato dell'elezione al drone successivo n° " + droneSuccessivo.getid());

    aggiornaTimestampLogicoInvioMessaggio();

    // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + droneSuccessivo.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    // Creo un blocco stub sul canale
    ElezioneReteRisultatoGrpc.ElezioneReteRisultatoBlockingStub stub =
        ElezioneReteRisultatoGrpc.newBlockingStub(canale);

    DroneNetwork.ElezioneReteRisultatoRequest richiesta =
        DroneNetwork.ElezioneReteRisultatoRequest.newBuilder()
            .setIdDroneMaster(this.id)
            .setTimestampLogico(getTimeStampLogico())
            .build();

    System.out.println(
        "Effettuo richiesta gRPC alla porta: " + droneSuccessivo.getPortaComunicazioneDroni());
    DroneNetwork.ElezioneReteRisultatoResponse risposta = stub.elezioneReteRisultato(richiesta);

    // Chiudo il canale
    canale.shutdown();
  }

  public void continuaPubblicazioneElezioneRisultato(int idMasterDrone) {

    if (sleepAttive) {
      try {
        System.out.println("\nCONTINUA PUBBLICAZIONE RISULTATO...\nATTENDO 8 SECONDI\n");
        Thread.sleep(8000);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    if (sonoMaster()) {
      System.out.println(
          "\nL'ID del messaggio è il mio ID: il messaggio contenente il risultato dell'elezione (io sono il master) ha terminato il giro.");

      try {
        Thread.sleep(5000); // Aspetto che tutti i droni mi inviino le loro posizioni.
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

    } else {
      setIdMasterDrone(idMasterDrone);
      // this.idMasterDrone = idMasterDrone;

      System.out.println(
          "\nL'ID del messaggio non è il mio ID: memorizzo il nuovo drone master che è il drone n° "
              + this.idMasterDrone);
      System.out.println("Mando il risultato dell'elezione al nodo successivo.");

      invioRisultatoElezioneNodoSuccessivo();

      System.out.println(
          "\nMando informazioni aggiornate del mio drone al drone master che è il drone n° "
              + this.idMasterDrone);

      invioInfoDroneAlMaster();
    }

    this.elezioneInCorso = false;
    synchronized (elezioneLock) {
      elezioneLock.notifyAll();
    }

    partecipanteElezione = false;
  }

  private void invioRisultatoElezioneNodoSuccessivo() {
    Drone_bean droneSuccessivo = droneListBean.getDroneSuccessivo(id, 0);

    aggiornaTimestampLogicoInvioMessaggio();

    // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + droneSuccessivo.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    // Creo un blocco stub sul canale
    ElezioneReteRisultatoGrpc.ElezioneReteRisultatoBlockingStub stub =
        ElezioneReteRisultatoGrpc.newBlockingStub(canale);

    DroneNetwork.ElezioneReteRisultatoRequest richiesta =
        DroneNetwork.ElezioneReteRisultatoRequest.newBuilder()
            .setIdDroneMaster(idMasterDrone)
            .setTimestampLogico(getTimeStampLogico())
            .build();

    System.out.println(
        "Effettuo richiesta gRPC alla porta: " + droneSuccessivo.getPortaComunicazioneDroni());
    DroneNetwork.ElezioneReteRisultatoResponse risposta = stub.elezioneReteRisultato(richiesta);

    // Chiudo il canale
    canale.shutdown();
  }

  private void invioInfoDroneAlMaster() {

    Drone_bean droneMaster = droneListBean.getbyid(idMasterDrone);

    if (droneMaster != null) {

      aggiornaTimestampLogicoInvioMessaggio();

      // Specifico indirizzo e porta del canale che offre il servizio SimpleSum
      final ManagedChannel canale =
          ManagedChannelBuilder.forTarget("localhost:" + droneMaster.getPortaComunicazioneDroni())
              .usePlaintext()
              .build();

      // Creo un blocco stub sul canale
      AggiornamentoInfoDroneGrpc.AggiornamentoInfoDroneBlockingStub stub =
          AggiornamentoInfoDroneGrpc.newBlockingStub(canale);

      DroneNetwork.AggiornamentoInfoDroneRequest richiesta =
          DroneNetwork.AggiornamentoInfoDroneRequest.newBuilder()
              .setIdDrone(id)
              .setLivelloBatteria(getLivelloBatteriaReale())
              .setCoordinataX(posizioneAttuale.getCoordinataAsseX())
              .setCoordinataY(posizioneAttuale.getCoordinataAsseY())
              .setTimestampLogico(getTimeStampLogico())
              .build();

      System.out.print(richiesta.toString() + "\n");

      System.out.println(
          "Effettuo richiesta gRPC alla porta: " + droneMaster.getPortaComunicazioneDroni());
      DroneNetwork.AggiornamentoInfoDroneResponse risposta = stub.aggiornamentoInfoDrone(richiesta);

      // Chiudo il canale
      canale.shutdown();
    }
  }

  public void aggiornaInfoDrone(
      int idDrone, int livelloBatteria, int coordinataX, int coordinataY) {
    Drone_bean drone = droneListBean.getbyid(idDrone);
    drone.setLivelloBatteria(livelloBatteria);
    drone.setPosizioneDroneCoordinataX(coordinataX);
    drone.setPosizioneDroneCoordinataY(coordinataY);
    System.out.println(
        "Aggiorno informazioni del drone n° "
            + drone.getid()
            + "\nDopo aggiornamento: \n"
            + drone.toString());
  }

  public Boolean sonoMaster() {
    return this.id == this.idMasterDrone;
  }

  public synchronized ArrayList<Ordine> getListaOrdini() {
    return listaOrdini;
  }

  public ArrayList<Ordine> getListaOrdiniCopy() {
    return new ArrayList<>(listaOrdini);
  }

  public void consegnaOrdine(Ordine ordine) {

    // Possiedo già il lock listaOrdiniInUtilizzo

    synchronized (ricaricaBatteriaLock) {
      while (ricaricaBatteriaInCorso) {
        try {

          ricaricaBatteriaLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    ricaricaBatteriaInCorso = false;

    System.out.println("Inizio la consegna dell'ordine n° " + ordine.getOrdineID());

    try {
      Thread.sleep(5000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nHo effettuato la consegna dell'ordine n° " + ordine.getOrdineID());

    getDroneListBean()
        .getbyid(id)
        .setLivelloBatteria(getDroneListBean().getbyid(id).getLivelloBatteria() - 10);
    System.out.println(
        "Il livello della mia batteria è: " + getDroneListBean().getbyid(id).getLivelloBatteria());

    chilometriEffettuatiUltimaStatistica = 0;
    chilometriEffettuatiUltimaStatistica +=
        CalcoloDistanzaDuePunti.effettuaCalcolo(posizioneAttuale, ordine.getPuntoRitiro());
    chilometriEffettuatiUltimaStatistica +=
        CalcoloDistanzaDuePunti.effettuaCalcolo(ordine.getPuntoRitiro(), ordine.getPuntoConsegna());

    chilometriEffettuatiTotali += chilometriEffettuatiUltimaStatistica;
    numeroTotaleConsegne += 1;

    posizioneAttuale = ordine.getPuntoConsegna();
    System.out.println("La mia posizione attuale è: \n" + posizioneAttuale.toString());

    eliminaOrdine(ordine.getOrdineID());

    droneListBean.getbyid(id).setIdOrdineInGestione(-1); // Sono di nuovo un drone libero

    ricaricaBatteriaInCorso = false;
    synchronized (ricaricaBatteriaLock) {
      ricaricaBatteriaLock.notifyAll();
    }
  }

  public synchronized void aggiungiOrdine(Ordine nuovoOrdine) {

    listaOrdini.add(nuovoOrdine);
  }

  public synchronized void eliminaOrdine(int ordineID) {

    listaOrdini.remove(this.getOrdineByID(ordineID)); // Rimuovo ordine
  }

  public synchronized Ordine getOrdineByID(int ordineID) {

    ArrayList<Ordine> listaOrdini = getListaOrdiniCopy();
    Ordine ordineRisultato = null;
    for (Ordine ordine : listaOrdini) {
      if (ordine.getOrdineID() == ordineID) {
        ordineRisultato = ordine;
      }
    }

    return ordineRisultato;
  }

  public void aggiornaStatistiche(
      int idDrone,
      int livelloBatteria,
      int posizioneCoordinataX,
      int posizioneCoordinataY,
      String timestamp,
      int chilometriEffettuati,
      double mediaMisurazioniInquinamento) {
    // Aggiorno Posizione Drone
    System.out.println("Aggiorno statistiche del drone n° " + idDrone);
    Drone_bean drone = droneListBean.getbyid(idDrone);
    drone.setPosizioneDroneCoordinataX(posizioneCoordinataX);
    drone.setPosizioneDroneCoordinataY(posizioneCoordinataY);
    drone.setLivelloBatteria(livelloBatteria);

    // Aggiorno Statistiche
    statisticheGlobali.setMediaNumeroConsegne(statisticheGlobali.getMediaNumeroConsegne() + 1);
    statisticheGlobali.setMediaLivelloInquinamento(
        (int) (statisticheGlobali.getMediaLivelloInquinamento() + mediaMisurazioniInquinamento));
    statisticheGlobali.setMediaChilometriEffettuati(
        statisticheGlobali.getMediaChilometriEffettuati() + chilometriEffettuati);
  }

  public PuntoMappa_bean getPosizioneAttuale() {
    return posizioneAttuale;
  }

  public int getChilometriEffettuatiUltimaStatistica() {
    return chilometriEffettuatiUltimaStatistica;
  }

  public void inviaStatisticheAlDroneMaster(int idOrdine) {

    if (getIdMasterDrone() != -1) {

      System.out.println(
          "\nInvio statistiche al master che è il drone n° " + getIdMasterDrone() + "\n");

      Drone_bean droneMaster = getDroneListBean().getbyid(getIdMasterDrone());

      aggiornaTimestampLogicoInvioMessaggio();

      final ManagedChannel canale =
          ManagedChannelBuilder.forTarget("localhost:" + droneMaster.getPortaComunicazioneDroni())
              .usePlaintext()
              .build();

      // Creo un blocco stub sul canale
      StatisticheAlDroneMasterGrpc.StatisticheAlDroneMasterBlockingStub stub =
          StatisticheAlDroneMasterGrpc.newBlockingStub(canale);

      Timestamp timestamp = new Timestamp(System.currentTimeMillis());

      DroneNetwork.StatisticheAlDroneMasterRequest richiesta =
          DroneNetwork.StatisticheAlDroneMasterRequest.newBuilder()
              .setIdDrone(getId())
              .setLivelloBatteria(getLivelloBatteriaReale())
              .setPosizioneCoordinataX(getPosizioneAttuale().getCoordinataAsseX())
              .setPosizioneCoordinataY(getPosizioneAttuale().getCoordinataAsseY())
              .setTimestamp(timestamp.toString())
              .setChilometriEffettuati(getChilometriEffettuatiUltimaStatistica())
              .setMediaMisurazioniInquinamento(getMediaMisurazioniInquinamento())
              .setIdOrdine(idOrdine)
              .setTimestampLogico(getTimeStampLogico())
              .build();

      System.out.print(richiesta.toString() + "\n");

      System.out.println(
          "Effettuo richiesta gRPC alla porta: " + droneMaster.getPortaComunicazioneDroni());
      DroneNetwork.StatisticheAlDroneMasterResponse risposta =
          stub.statisticheAlDroneMaster(richiesta);

      // Chiudo il canale
      canale.shutdown();
    }
  }

  private double getMediaMisurazioniInquinamento() {

    return mediaMisurazioniInquinamento;
  }

  public void calcolaMediaMisurazioniInquinamento() {
    List<Measurement> misurazioni = getSensoreInquinamentoBuffer().readAllAndClean();

    double mediaMisurazioni = 0;
    int numeroMisurazioni = 0;

    for (Measurement misurazione : misurazioni) {

      mediaMisurazioni += misurazione.getValue();
      numeroMisurazioni += 1;
    }

    mediaMisurazioni = mediaMisurazioni / numeroMisurazioni;
    setMediaMisurazioniInquinamento(mediaMisurazioni);
  }

  public void setMediaMisurazioniInquinamento(double mediaMisurazioniInquinamento) {
    this.mediaMisurazioniInquinamento = mediaMisurazioniInquinamento;
  }

  public StatisticheGlobali_bean getStatisticheGlobaliCopy() {
    return new StatisticheGlobali_bean(
        statisticheGlobali.getMediaNumeroConsegne(),
        statisticheGlobali.getMediaLivelloBatteriaDroni(),
        statisticheGlobali.getMediaChilometriEffettuati(),
        statisticheGlobali.getMediaLivelloInquinamento(),
        statisticheGlobali.getTimestamp());
  }

  public void azzeraStatisticheGlobali() {
    statisticheGlobali = new StatisticheGlobali_bean();
  }

  public int calcolaMediaLivelloBatteriaDroni() {
    int mediaBatteria = 0;
    for (Drone_bean drone : getDroneListBean().getArrayListDrones()) {
      mediaBatteria += drone.getLivelloBatteria();
    }

    mediaBatteria = mediaBatteria / getDroneListBean().countdrones();

    return mediaBatteria;
  }

  public int inviaStatisticheAlServer(StatisticheGlobali_bean statisticheGlobali) {
    Client client = Client.create();
    ClientResponse clientResponse = null;

    // Mando informazioni del drone attuale al Server tramite richiesta POST.
    String postPath = "/gestioneDroni/aggiungiStatistiche";
    clientResponse =
        postRequestStatistiche(
            client, indirizzoServerAmministratore + postPath, statisticheGlobali);
    System.out.println("Risposta del Server REST: " + clientResponse.toString());
    return clientResponse.getStatus();
  }

  public SensoreInquinamentoBuffer getSensoreInquinamentoBuffer() {
    return sensoreInquinamentoBuffer;
  }

  public void tentaRicaricaBatteria() {

    System.out.println("\nTento di ricaricare la batteria...");

    richiestaRicaricaBatteria = true;

    for (Drone_bean drone : getDroneListBean().getArrayListDrones()) {
      System.out.println(
          "Aggiunto il drone n° "
              + drone.getid()
              + " alla lista dei droni di cui attendo la conferma.");
      droniDiCuiAttendoConferma.add(drone.getid());
    }
    for (Drone_bean drone : getDroneListBean().getArrayListDrones()) {
      System.out.println("\nInvio messaggio al drone n° " + drone.getid() + ".");

      aggiornaTimestampLogicoInvioMessaggio();

      final ManagedChannel canale =
          ManagedChannelBuilder.forTarget("localhost:" + drone.getPortaComunicazioneDroni())
              .usePlaintext()
              .build();

      RicaricaBatteriaRichiestaGrpc.RicaricaBatteriaRichiestaBlockingStub stub =
          RicaricaBatteriaRichiestaGrpc.newBlockingStub(canale);

      DroneNetwork.RicaricaBatteriaRichiestaRequest richiesta =
          DroneNetwork.RicaricaBatteriaRichiestaRequest.newBuilder()
              .setIdDrone(getId())
              .setTimestampLogico(getTimeStampLogico())
              .build();

      System.out.println(
          "Effettuo richiesta gRPC alla porta: " + drone.getPortaComunicazioneDroni());
      DroneNetwork.RicaricaBatteriaRichiestaResponse risposta =
          stub.ricaricaBatteriaRichiesta(richiesta);

      // Chiudo il canale
      canale.shutdown();
    }
  }

  public synchronized void aggiornaTimestampLogicoInvioMessaggio() {
    timeStampLogico += 1;
  }

  public int getTimeStampLogico() {
    return timeStampLogico;
  }

  public synchronized void aggiornaTimestampLogicoRicezioneMessaggio(int timestampAltroNodo) {

    if (timeStampLogico > timestampAltroNodo) {

      timeStampLogico = timeStampLogico + 1;

    } else {
      timeStampLogico = timestampAltroNodo + 1;
    }
  }

  public ArrayList<Integer> getDroniDiCuiAttendoConferma() {
    return droniDiCuiAttendoConferma;
  }

  public ArrayList<Integer> getDroniACuiDevoInviareConferma() {
    return droniACuiDevoInviareConferma;
  }

  public void inviaConfermaRichiestaBatteria(int idDrone) {

    Drone_bean drone = getDroneListBean().getbyid(idDrone);

    System.out.println(
        "Invio OK per la ricaricare della batteria al drone n° " + drone.getid() + ".");

    final ManagedChannel canale =
        ManagedChannelBuilder.forTarget("localhost:" + drone.getPortaComunicazioneDroni())
            .usePlaintext()
            .build();

    RicaricaBatteriaRispostaGrpc.RicaricaBatteriaRispostaBlockingStub stub =
        RicaricaBatteriaRispostaGrpc.newBlockingStub(canale);

    aggiornaTimestampLogicoInvioMessaggio();

    DroneNetwork.RicaricaBatteriaRispostaRequest richiesta =
        DroneNetwork.RicaricaBatteriaRispostaRequest.newBuilder()
            .setIdDrone(getId())
            .setTimestampLogico(getTimeStampLogico())
            .build();

    // System.out.print(richiesta.toString() + "\n");

    System.out.println("Effettuo richiesta gRPC alla porta: " + drone.getPortaComunicazioneDroni());
    DroneNetwork.RicaricaBatteriaRispostaResponse risposta =
        stub.ricaricaBatteriaRisposta(richiesta);

    // Chiudo il canale
    canale.shutdown();
  }

  public void ricaricaBatteria() {

    // Aspetto l'ultima consegna
    System.out.println("Aspetto ultima consegna prima di ricaricare batteria...");
    aspettaConsegnaInCorso();
    System.out.println("\nUltima consegna effettuata.");

    if (!sonoMaster()) {

      // Setto a 0 la batteria e invio l'informazione al master così non mi può più assegnare un
      // ordine
      getDroneListBean().getbyid(id).setLivelloBatteria(0);
      invioInfoDroneAlMaster();
    } else {
      // Se sono il master evito che il Thread AssegnaOrdini mi veda come un drone libero.
      getDroneListBean().getbyid(id).setIdOrdineInGestione(0);
    }

    // Ricarico la batteria

    synchronized (ricaricaBatteriaLock) {
      while (ricaricaBatteriaInCorso) {
        try {

          ricaricaBatteriaLock.wait();

        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
    ricaricaBatteriaInCorso = true;

    System.out.println("\nInizio ricarica della batteria...\n");

    try {
      Thread.sleep(10000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    System.out.println("\nRicarica della batteria conclusa.\n");

    // Salvo la mia nuova posizione.

    posizioneAttuale.setCoordinataAsseX(0);
    posizioneAttuale.setCoordinataAsseY(0);

    // Setto il nuovo livello di batteria
    getDroneListBean().getbyid(id).setLivelloBatteria(100);

    // Se sono il master già possiedo le informazioni, altrimenti comunico la nuova posizione e il
    // nuovo livello di batteria al master.

    if (!sonoMaster()) {
      invioInfoDroneAlMaster();
    } else {
      getDroneListBean().getbyid(id).setIdOrdineInGestione(-1);
    }

    richiestaRicaricaBatteria = false;
    ricaricaBatteriaInCorso = false;

    ricaricaBatteriaInCorso = false;
    synchronized (ricaricaBatteriaLock) {
      ricaricaBatteriaLock.notifyAll();
    }

    // Comunico ai droni in attesa del mio OK che la risorsa è liberata.

    for (int idDrone : droniACuiDevoInviareConferma) {

      System.out.println(
          "Invio OK al drone n° " + idDrone + " rimasto in attesa di ricaricare la batteria.");

      Drone_bean drone = getDroneListBean().getbyid(idDrone);

      final ManagedChannel canale =
          ManagedChannelBuilder.forTarget("localhost:" + drone.getPortaComunicazioneDroni())
              .usePlaintext()
              .build();

      RicaricaBatteriaRispostaGrpc.RicaricaBatteriaRispostaBlockingStub stub =
          RicaricaBatteriaRispostaGrpc.newBlockingStub(canale);

      aggiornaTimestampLogicoInvioMessaggio();

      DroneNetwork.RicaricaBatteriaRispostaRequest richiesta =
          DroneNetwork.RicaricaBatteriaRispostaRequest.newBuilder()
              .setIdDrone(getId())
              .setTimestampLogico(getTimeStampLogico())
              .build();

      System.out.print(richiesta.toString() + "\n");

      System.out.println(
          "Effettuo richiesta gRPC alla porta: " + drone.getPortaComunicazioneDroni());
      DroneNetwork.RicaricaBatteriaRispostaResponse risposta =
          stub.ricaricaBatteriaRisposta(richiesta);

      // Chiudo il canale
      canale.shutdown();
    }
  }

  public int getChilometriEffettuatiTotali() {
    return chilometriEffettuatiTotali;
  }

  public int getNumeroTotaleConsegne() {
    return numeroTotaleConsegne;
  }

  public int getPortaComunicazioneDroni() {
    return portaComunicazioneDroni;
  }

  public String getIndirizzoDrone() {
    return indirizzoDrone;
  }
}
