package Drone;

import Simulatore.Buffer;
import Simulatore.Measurement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SensoreInquinamentoBuffer implements Buffer {

  private final DroneMain droneMain;

  public SensoreInquinamentoBuffer(DroneMain drone) {
    droneMain = drone;
  }

  private List<Measurement> bufferDati = new ArrayList<>();

  @Override
  public void addMeasurement(Measurement m) {
    bufferDati.add(m);

    if (bufferDati.size() == 8) {
      droneMain.calcolaMediaMisurazioniInquinamento();
    }
  }

  @Override
  public List<Measurement> readAllAndClean() {
    List<Measurement> slide = bufferDati;
    bufferDati = bufferDati.stream().skip(4).collect(Collectors.toList());
    return slide;
  }
}
