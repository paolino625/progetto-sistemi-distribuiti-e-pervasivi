# CASI LIMITE SERVER REST

## Multipli droni cercano di entrare/uscire in maniera concorrente

Se due droni entrano nello stesso tempo nella rete, il server non riesce a valutare bene se l'ID è univoco o meno.

Soluzione: Il server Rest chiama la funzione add sul dronesList.
Quest'ultima funzione (marcata come synchronized) controllerà se è già presente un drone con lo stesso ID.
In caso positivo non fa nulla e restituisce false, altrimenti aggiunge il drone alla lista e ritorna true.

## Multipli droni lasciano la rete in maniera concorrente

Non ci sono problemi.

## Un drone sta cercando di entrare con l'ID di un drone uscente

Non c'è pericolo di sincronizzazione.

Se viene eseguita prima la funzione removebyid sull'arrayList, allora il drone entrante può entrare con lo stesso ID.
Se viene eseguita prima la funzione add sull'arrayList, allora il drone entrante non riuscirà ad entrare.
Se le due funzioni vengono eseguite nello stesso tempo, non ci sono comunque problemi di concorrenza: nel peggiore dei casi mentre viene eliminato il drone uscente dall'arrayList, viene effettuata un'operazione di get sull'array nell'if (quindi solo lettura).