# CASI LIMITE ELEZIONE

## Uscita drone durante elezione

### Il nodo candidato master esce dalla rete prima di ricevere l'ultimo messaggio di election

Esempio: in una rete con tre processi con ID 1, 2, 3: il drone 1 inizia l'elezione, il messaggio di Election arriva al drone 3 che sostituisce l'ID del messaggio con il suo ID.
  Dopodiché inoltra il messaggio al nodo 1 e poi esce dalla rete.
  Il messaggio di election continuerà a girare a vuoto perché non ci sarà il drone 3 che fermerà il messaggio di Election.
  Soltanto nel caso in cui un drone con un ID maggiore di 3 dovesse entrare nella rete la situazione si sbloccherebbe.

Soluzione: impedire l'uscita dei droni durante un'elezione: si aspetta la fine dell'elezione per uscire.

### Il nodo candidato master esce dalla rete prima di ricevere l'ultimo messaggio elected

Esempio: il drone 3 manda il messaggio Elected e poi esce dalla rete.
Il messaggio continua a circolare perché nessuno lo ferma.
Il drone 2 però setta il master e inizia a pingarlo (ogni drone pinga il drone successivo): appena si accorge della sua assenza indice una nuova elezione.

## Entrata nuovo drone durante elezione

### Messaggio Election

#### Messaggio Election ha già superato l'ID del drone in entrata

Un nuovo drone entra mentre c'è un'elezione.
Il messaggio election sta circolando nella rete (oppure è appena tornato al candidato master) ma il candidato master non ha ancora inviato il messaggio elected.
Il nodo successivo del nodo appena entrato, inoltra a quest'ultimo l'ID del vecchio drone Master.
Tuttavia non c'è da preoccuparsi: quando il candidato master inizia a far circolare il messaggio elected, questo arriverà sicuramente anche al nuovo nodo che impsoterà l'ID del nuovo master correttamente.

Esempio.
Mettiamo che sono già presenti nella rete i droni 1,2,3 ed entra il drone 4.
Se il messaggio elected non è ancora partito dal nodo 3, allora il nodo candidato master 3 si accorgerà dell'entrata del drone 4 e lo imposterà come drone successivo; inviandogli dunque il messaggio elected.

#### Messaggio Election deve ancora arrivare all'ID del drone in entrata 

Un nuovo drone entra mentre c'è un'elezione.
Il nodo successivo gli comunica l'ID del drone Master che è quello vecchio in quanto non gli è ancora arrivato il messaggio election.
Il drone riceve il messaggio election e può partecipare attivamente all'elezione.

### Messaggio Elected

#### Messaggio Elected ha già superato l'ID del drone in entrata 

Un nuovo drone entra mentre c'è un'elezione.
Il messaggio elected sta circolando nella rete e nello specifico ha già superato l'ID del drone in entrata.
Il nodo successivo del nodo in entrata avrà ricevuto il messaggio elected e potrà comunicargli l'ID del master corretto.

Mettiamo che sono già presenti nella rete i droni 1,2,3 ed entra il drone 4.
Il messaggio elected è arrivato al drone 2 e sta per essere inviato al nodo 3.
Dunque il nodo 1 ha già ricevuto il messaggio elected e settato il nuovo master.
Dunque il nodo 1, dopo la presentazione del nodo 4, potrà comunicargli l'ID del master, ovvero l'ID 3.

#### Messaggio Elected deve ancora arrivare all'ID del drone in entrata

Un nuovo drone entra mentre c'è un'elezione.
Il messaggio elected sta circolando nella rete e nello specifico non ha ancora superato l'ID del drone in entrata.
Il drone precedente si accorgerà dell'entrata del nuovo drone e gli invierà il messaggio elected.

#### Caso particolare

Mettiamo che sono già presenti nella rete i droni 1,2,3 ed entra il drone 4.

Potrebbe succedere il raro caso in cui il nodo 4 invia il messaggio di presentazione nell'esatto momento in cui il nodo 3 invia il messaggio al nodo 1.
In questo caso il nodo 4 non riceverà mai il messaggio elected e il drone 1 risponde al nodo 4 prima di ricevere il messaggio elected.

Soluzione: ogni drone, prima di accettare un messaggio di presentazione di un drone che vuole entrare nella rete, rimane in attesa che finisca l'elezione.
In questo modo questo caso non può verificarsi.

