# PRESENTAZIONE PROGETTO

- Verrà verificato il corretto funzionamento del programma (sarà richiesto di testare il funzionamento del sistema avviando almeno 4 o 5 droni).
- Verranno poste alcune domande di carattere teorico inerenti agli argomenti trattati nella parte di laboratorio.

## SCHEMA

- Si consiglia vivamente di analizzare con attenzione tutte le problematiche di sincronizzazione e di rappresentare lo schema di comunicazione fra le componenti.
- Questo schema deve rappresentare il formato dei messaggi e la sequenza delle comunicazioni che avvengono tra le componenti in occasione delle varie operazioni che possono essere svolte.
- Tale schema sarà di grande aiuto, in fase di presentazione del progetto, per verificare la correttezza della parte di sincronizzazione distribuita.

## CONSIGLIO

- Si invitano inoltre gli studenti ad utilizzare, durante la presentazione, le istruzioni Thread.sleep() al ﬁne di mostrare la correttezza della sincronizzazione del proprio programma.
