# SEMPLIFICAZIONI

- Non è necessario gestire le consegne generate da Dronazon nell'intervallo di tempo che intercorre tra l'uscita dalla rete del drone master corrente e l'elezione del successivo.

- Si assume che un drone porta sempre a termine con successo la consegna a lui assegnata.

- Nessun nodo si comporta in modo malizioso.

- Nessun processo termina in maniera incontrollata.

# VINCOLI

- Si gestiscano invece i possibili errori di inserimento dati da parte dell’utente.

- Inoltre, il codice deve essere robusto: tutte le possibili eccezioni devono essere gestite correttamente.

- Per la comunicazione tra processi è necessario utilizzare il framework gRPC. 
  Qualora fossero previste comunicazioni in broadcast, queste devono essere effettuate in parallelo e non sequenzialmente.