# CONSEGNA

La consegna deve essere tassativamente effettuata entro le 23:59 del secondo giorno precedente quello della discussione (es. esame il 13 mattina, consegna entro le 23.59 dell’11).  Non saranno accettate eventuali modifiche apportate al progetto dopo il termine della consegna.
Nel mio caso: entro le 23.59 del 19 Settembre.

È sufficiente archiviare il codice in un file zip, rinominato con il proprio numero di matricola (es. 760936.zip) ed effettuare l’upload dello stesso tramite il sito http://upload.di.unimi.it
