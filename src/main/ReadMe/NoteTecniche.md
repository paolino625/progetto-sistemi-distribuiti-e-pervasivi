# NOTE PAOLO

## Jersey/Rest (Classi bean)

- Non inserire lettere maiuscole o underscore nei nomi delle variabili e metodi, altrimenti Jersey impazzisce e non deserializza bene i JSON.
- Mettere get/set di ogni attributo con la nomenclatura suggerita da IntelliJ, altrimenti Jersey non funziona bene.

## Protobuf

- Non utilizzare campi nominati come "isQualcosa".
- Nei file proto inserire sempre il package altrimenti ci sono problemi nella build.